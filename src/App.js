import React, {useState} from "react";
import Home from "./component/Home/Home";
import Header from "./component/Header/Header";
import {Route, Switch} from "react-router-dom";
import {UserContext} from "./util/UserContext";
import store from "./util/store";

import i18 from './i18n'
import Social from "./component/Social";
import Test from "./component/Test";
import About from "./component/About";
import Contact from "./component/Contact/Contact";
import Service from "./component/Service/Service";
import Portfolio from "./component/Portfolio/Portfolio";
import Project1 from "./component/Portfolio/Project1";
import Project2 from "./component/Portfolio/Project2";
import News from "./component/News/News";
import NewsSingle from "./component/NewsSingle/NewsSingle";
import YearToOne from "./component/Baby/YearToOne";
import YearToFive from "./component/Baby/YearToFive";
import YearToFourteen from "./component/Baby/YearToFourteen";
import YearToEighteen from "./component/Baby/YearToEighteen";
import Religion from "./component/Advice/Religion";
import Psychology from "./component/Advice/Psychology";
import Medical from "./component/Health/Medical";
import TrimestrOne from "./component/Health/Trimestr/TrimestrOne";
import TrimestrTwo from "./component/Health/Trimestr/TrimestrTwo";
import TrimestrThree from "./component/Health/Trimestr/TrimestrThree";
import Baby from "./component/Health/Baby";
import MilkBaby from "./component/Health/Baby/MilkBaby";
import YoungPupil from "./component/Health/Baby/YoungPupil";
import TeenagePeriod from "./component/Health/Baby/TeenagePeriod";
import Advise from "./component/Advice/Advise";
import Facility from "./component/StateAndFamily/Lgoti";
import Credit from "./component/StateAndFamily/Kredit";
import Law from "./component/Advice/Law";
import PeriodPregnancy from "./component/Health/Baby/PeriodPregnancy";
import Marriage from "./component/Advice/Marriage";
import HealthLife from "./component/Health/LifeStyle/HealthLife";
import Virus from "./component/Health/LifeStyle/Virus";
import Clinic from "./component/Health/LifeStyle/Clinic";
import OldMen from "./component/Health/LifeStyle/OldMen";
import Higienic from "./component/Health/LifeStyle/Higienic";
import Diseas from "./component/Health/LifeStyle/Diseas";
import ImproperNutrition from "./component/Health/LifeStyle/ImproperNutrition";
import Footer from "./component/Footer/Footer";

function App() {
    const [top, setTop] = useState(10);
    const [bottom, setBottom] = useState(10);
    const [news, setNews] = useState(store);

    const handleLanguage = (lng) => {
        i18.changeLanguage(lng)
        console.log(lng)
    }
    return (
        <div>
            <UserContext.Provider value={{news, setNews}}>

                <Header/>
                <Social/>
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <Route exact path="/test" component={Test}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/contact" component={Contact}/>
                    <Route exact path="/service" component={Service}/>
                    <Route exact path="/portfolio" component={Portfolio}/>
                    <Route exact path="/project1" component={Project1}/>
                    <Route exact path="/project2" component={Project2}/>
                    <Route exact path="/news" component={News}/>
                    <Route exact path="/newsSingle" component={NewsSingle}/>
                    <Route exact path="/yearToOne" component={YearToOne}/>
                    <Route exact path="/yearToFive" component={YearToFive}/>
                    <Route exact path="/yearToFourteen" component={YearToFourteen}/>
                    <Route exact path="/yearToEighteen" component={YearToEighteen}/>
                    <Route exact path="/religion" component={Religion}/>
                    <Route exact path="/psychology" component={Psychology}/>
                    <Route exact path="/medical" component={Medical}/>
                    <Route exact path="/trimestrOne" component={TrimestrOne}/>
                    <Route exact path="/trimestrTwo" component={TrimestrTwo}/>
                    <Route exact path="/trimestrThree" component={TrimestrThree}/>
                    <Route exact path="/baby" component={Baby}/>
                    <Route exact path="/milkBaby" component={MilkBaby}/>
                    <Route exact path="/youngPupil" component={YoungPupil}/>
                    <Route exact path="/teenagePeriod" component={TeenagePeriod}/>
                    <Route exact path="/advise" component={Advise}/>
                    <Route exact path="/facility" component={Facility}/>
                    <Route exact path="/credit" component={Credit}/>
                    <Route exact path="/law" component={Law}/>
                    <Route exact path="/periodPregnancy" component={PeriodPregnancy}/>
                    <Route exact path="/marriage" component={Marriage}/>
                    <Route exact path="/healthLife" component={HealthLife}/>
                    <Route exact path="/virus" component={Virus}/>
                    <Route exact path="/clinic" component={Clinic}/>
                    <Route exact path="/oldMen" component={OldMen}/>
                    <Route exact path="/higienic" component={Higienic}/>
                    <Route exact path="/diseas" component={Diseas}/>
                    <Route exact path="/improperNutriton" component={ImproperNutrition}/>
                    <Route exact path='*'>
                        <Home/>
                    </Route>
                </Switch>
                <Footer/>
            </UserContext.Provider>
        </div>
    );
}

export default App;

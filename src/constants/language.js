const list = {
  // en: { title: "English" },
  ru: { title: "Русский" },
  // uz: { title: "O'zbek" },
  oz: { title: "Ўзбек" }
};

const defaultValue = "ru";

export default {
  list,
  defaultValue
};

export default {

  signUp: 'POST /auth/register',
  signIn: 'POST /auth/login',
  editCabinet: 'PUT /auth',

  userMe: '/company/me',

  uploadFile: 'POST /attach',

  getCountries: '/country',
  getCountry: '/country',
  addCountry: 'POST /country',
  editCountry: 'PUT /country',
  deleteCountry: 'DELETE /country',

  getRegions: '/region',
  getRegion: '/region',
  addRegion: 'POST /region',
  editRegion: 'PUT /region',
  deleteRegion: 'DELETE /region',
  filterCountryByRegion: '/region/search/filterByCountry',

  getDistricts: '/district',
  getDistrict: '/district',
  addDistrict: 'POST /district',
  editDistrict: 'PUT /district',
  deleteDistrict: 'DELETE /district',
  filterDistrictsByRegion: '/district/search/filterByRegion',

  getPhoneNumberTypes: '/phoneNumberType',
  getPhoneNumberType: '/phoneNumberType',
  addPhoneNumberType: 'POST /phoneNumberType',
  editPhoneNumberType: 'PUT /phoneNumberType',
  deletePhoneNumberType: 'DELETE /phoneNumberType',

  getAwares: '/aware',
  getAware: '/aware',
  addAware: 'POST /aware',
  editAware: 'PUT /aware',
  deleteAware: 'DELETE /aware',

  addCompany: 'POST /company',
  getCompanies: '/company',
  getCompany: '/company',
  deleteCompany: 'DELETE /company',
  exportExcel: '/company/exportExcel',
  getMyCompanies: '/company/getMyCompanies',

  addTourPackage:'POST /tour',
  foundPackages: '/tour/searchPackage',
  getMytourPackages: '/tour/getMytourPackages',
  getTourpackage:'/tour',
  editTourPackage:'PUT /tour',
  deleteTourPackage:'DELETE /tour',
}


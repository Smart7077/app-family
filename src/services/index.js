// import { apiPrefix } from 'utils/config'

import api from './api'
import request from "../util/request";

const gen = params => {
    let url = params
    let method = 'GET'

    const paramsArray = params.split(' ')
    if (paramsArray.length === 2) {
        method = paramsArray[0]
        url = paramsArray[1]
    }

    return function (data) {
        return request({
            url,
            data,
            method,
        })
    }
}

const APIFunction = {}
for (const key in api) {
    APIFunction[key] = gen(api[key])
}

export default APIFunction

//

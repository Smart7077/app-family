import {Link, withRouter} from 'react-router-dom';
import './header.css'
import React, {useEffect, useState} from "react";
import i18 from '../../i18n'

function Header() {

    const [offset, setOffset] = useState(0);

    useEffect(() => {
        window.onscroll = () => {
            setOffset(window.pageYOffset)
        }
    }, []);

    const handleLanguage = (e) => {

        i18.changeLanguage(e.target.value)
    }

    return (
        <div>
            <div id="wrapper" className="animsition">
                <div id="page" className="clearfix">
                    <div id="site-header-wrap" className="absolute">
                        <header id="site-header">
                            <div className="wrap-themesflat-container">
                                <div id="site-header-inner" className="themesflat-container">
                                    <div className="wrap-inner">
                                        <div id="site-logo"
                                             className="clearfix logoPush">
                                            <div id="site-logo-inner">
                                                <a href="/" title="Finance" rel="home"
                                                   className="main-logo"><img
                                                    src={offset > 1 ? `assets/img/logoBlue.png` : `assets/img/logoWhite.png`}
                                                    width="184" height="40" alt="Finance"
                                                    data-retina="assets/img/logo@2x.png" data-width="184"
                                                    data-height="40"/></a>
                                            </div>
                                        </div>

                                        <div className="mobile-button"><span></span></div>

                                        <nav id="main-nav" className="main-nav">
                                            <ul id="menu-primary-menu" className="menu">
                                                <li className="menu-item  menu-item-has-children text-uppercase">
                                                    <a
                                                    >Государство и семья</a>
                                                    <ul className="sub-menu">
                                                        <li className="menu-item">
                                                            <a href="/facility">Льготы</a>
                                                        </li>
                                                        <li className="menu-item"><a href="/credit">Кредиты</a>
                                                        </li>
                                                    </ul>
                                                </li>


                                                <li className="menu-item menu-item-has-children text-uppercase"><a
                                                >Детское
                                                    воспитание</a>
                                                    <ul className="sub-menu">
                                                        <li className="menu-item"><a href="/yearToOne">Возраст
                                                            (0-1)</a></li>
                                                        <li className="menu-item"><a href="/yearToFive">Возраст
                                                            (1-5)</a></li>
                                                        <li className="menu-item"><a href="/yearToFourteen">Возраст
                                                            (6-14)</a></li>
                                                        <li className="menu-item"><a href="/yearToEighteen">Возраст
                                                            (15-17)</a></li>
                                                    </ul>
                                                </li>
                                                <li o className="menu-item menu-item-has-children text-uppercase"><a
                                                >Советы</a>
                                                    <ul className="sub-menu">
                                                        <li className="menu-item"><a href="/marriage">“ЗАГС”</a>
                                                        </li>
                                                        <li className="menu-item"><a href="/law">Право</a>
                                                        </li>
                                                        <li className="menu-item"><a href="/religion">Религия</a>
                                                        </li>
                                                        <li className="menu-item"><a
                                                            href="/psychology">Психология</a></li>

                                                    </ul>
                                                </li>

                                                <li className="menu-item  menu-item-has-children text-uppercase"><a
                                                >Здоровье</a>
                                                    <ul className="sub-menu">
                                                        <li className="menu-item"><a href="/medical">Пары
                                                            (мед осмотр)</a></li>
                                                        <li className="menu-item menu-item-has-children"><a
                                                            href="/periodPregnancy"
                                                        >Период беременности</a>
                                                            <ul className="sub-menu">
                                                                <li className="menu-item"><a href="/trimestrOne">1
                                                                    Триместр</a></li>
                                                                <li className="menu-item"><a href="/trimestrTwo">2
                                                                    Триместр</a></li>
                                                                <li className="menu-item"><a href="/trimestrThree">3
                                                                    Триместр</a></li>

                                                            </ul>
                                                        </li>

                                                        <li className="menu-item  menu-item-has-children"><a
                                                            href="/baby"
                                                        >Ребенок</a>
                                                            <ul className="sub-menu">
                                                                <li className="menu-item"><a
                                                                    href="/milkBaby">Грудной ребёнок</a>
                                                                </li>
                                                                <li className="menu-item"><a
                                                                    href="/youngPupil">Младший школьник</a>
                                                                </li>
                                                                <li className="menu-item"><a
                                                                    href="/teenagePeriod">Подростковый
                                                                    период</a></li>
                                                            </ul>
                                                        </li>
                                                        <li className="menu-item  menu-item-has-children"><a
                                                            href="/healthLife"
                                                        >Здоровый образ жизни</a>
                                                            <ul className="sub-menu">
                                                                <li className="menu-item"><a
                                                                    href="/virus">Коронавирус</a>
                                                                </li>
                                                                <li className="menu-item"><a
                                                                    href="/clinic">Клиника</a>
                                                                </li>
                                                                <li className="menu-item"><a
                                                                    href="/oldMen">Старения</a></li>
                                                                <li className="menu-item"><a
                                                                    href="/higienic">Гигиена</a>
                                                                </li>
                                                                <li className="menu-item"><a
                                                                    href="/diseas">Болезни</a>
                                                                </li>
                                                                <li className="menu-item"><a
                                                                    href="/improperNutriton">Неправилная питания</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>


                                                <li className="menu-item menu-item-has-children text-uppercase"><a
                                                    href="/news">Новости</a>
                                                </li>
                                            </ul>

                                        </nav>


                                        {/*        <div className="nav-top-cart-wrapper">*/}
                                        {/*            <a className="nav-cart-trigger" href="page-shop-cart.html">*/}
                                        {/*<span className="cart-icon finance-icon-shopping-cart">*/}
                                        {/*   +99871 1000000*/}
                                        {/*</span>*/}
                                        {/*            </a>*/}
                                        {/*        </div>*/}


                                        {/*<div className="nav-top-cart-wrapper" style={{marginRight: -100, marginTop: 5}}>*/}
                                        {/*    <select style={{border: 0, color: 'white'}} name="language" id="setLan">*/}
                                        {/*        <option value="Uz">Uz</option>*/}
                                        {/*        <option value="Ru">Ru</option>*/}
                                        {/*    </select>*/}
                                        {/*</div>*/}
                                        <div className="nav-top-cart-wrapper ui-select"
                                             style={{float: 'right', marginTop: 5, marginRight: -100, width: '100px'}}>
                                            <select style={{border: 0, color: 'white'}} onChange={handleLanguage}>
                                                <option value="ru" style={{color: 'black'}}>Ru</option>
                                                <option value="uz" style={{color: 'black'}}>Uz</option>
                                            </select>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </header>
                    </div>

                </div>
            </div>

        </div>
    )
}

export default withRouter(Header);
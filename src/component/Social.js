import React from 'react';
import {useHistory} from "react-router-dom";

function Social() {
    const history = useHistory();

    return (
        <div className="affix-layout">
            <div className="affix-content">
                <button className="affix-content-button4 affix-p-link">
                    <a href="https://instagram.com/yoshoila_uz?igshid=gcz4zns6qck" target="_blank"><img
                        src="assets/img/social/instagram.png" alt=""/></a>
                </button>
                <button className="affix-content-button affix-p-link">
                    <a href="https://vm.tiktok.com/ZSJDx9Hm4/" target="_blank">
                        <img src="assets/img/social/tiktok.png" alt=""/>
                    </a>
                </button>
                {/*ssas*/}
                <button className="affix-content-button1 affix-p-link">
                    <a href="https://www.youtube.com/channel/UCfWPwvda7JSbk7hMBcGg0Xg" target="_blank"><img
                        src="assets/img/social/youtube.png" alt=""/></a>
                </button>
                <button className="affix-content-button2 affix-p-link">
                    <a target="_blank"
                       href="https://l.facebook.com/l.php?u=https%3A%2F%2Ffb.me%2Fyoshoilaplatformasi&h=AT15Wep5lDjDAoy3WrcFSbKDnhlW8C6822dzqUHG0QCaBIvskehM2idAr4v91KzVAzV3fF7boBg7264fH5MOlymduo5RJ7ObFaXMd43ac36Def7HtDKWYRpwp1PoGGrZ0bgYdw">
                        <img src="assets/img/social/facebook.png" alt=""/></a>
                </button>
                <button className="affix-content-button3 affix-p-link">
                    <a target="_blank" href="https://t.me/yosh_oila_platformasi">
                        <img src="assets/img/social/telegram.png" alt=""/>
                    </a>

                </button>
            </div>
        </div>
    );
}

export default Social;
import NewsSingleFeaturedTitle from "./NewsSingleFeaturedTitle";
import NewsSingleHeroSection from "./NewsSingleHeroSection";
import NewsSingleSideBar from "./NewsSingleSideBar";
import NewsSingleSiteContent from "./NewsSingleSiteContent";
import {useContext} from "react";
import {UserContext} from "../../util/UserContext";

const NewsSingle = () => {

    const newsId = (localStorage.getItem('newsId'));

    const {news} = useContext(UserContext);
    const currentNews = news[newsId];
    console.log()

    return (
        <div>
            <div id="wrapper" className="animsition">
                <div id="page" className="clearfix">
                    <div id="content-wrap">
                        {/* <NewsSingleHeroSection/> */}
                        <div style={{ height: '100px', backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></div>
                        <NewsSingleFeaturedTitle/>
                        <div id="main-content" className="site-main clearfix">

                            <div id="site-content" className="site-content clearfix">
                                <div id="inner-content" className="inner-content-wrap">
                                    <div className="page-content">

                                        <div className="row-accordion">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="themesflat-spacer clearfix" data-desktop="91"
                                                             data-mobi="60" data-smobi="60"></div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <NewsSingleSiteContent currentnews={currentNews}/>
                                                    <NewsSingleSideBar/>
                                                </div>

                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="themesflat-spacer clearfix" data-desktop="91"
                                                             data-mobi="60" data-smobi="60"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default NewsSingle;
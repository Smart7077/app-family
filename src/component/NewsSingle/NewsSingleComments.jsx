import React from 'react';

const NewsSingleComments = () => {
    return (
        <div id="comments" className="comments-area">
            <h2 className="comments-title">Комментари</h2>
            <ol className="comment-list">
                <li className="comment">
                    <article
                        className="comment-wrap clearfix">
                        <div className="gravatar"><img
                            alt="image"
                            src="assets/img/avatar.png" />
                        </div>
                        <div
                            className="comment-content">
                            <div
                                className="comment-meta">
                                <h6 className="comment-author">Анна</h6>
                                <span
                                    className="comment-time">Декабр 30, 2020</span>
                                <span
                                    className="comment-reply"><a
                                        className="comment-reply-link"
                                        href="#">Ответить</a></span>
                            </div>
                            <div
                                className="comment-text">
                                <p>Подскажите, пожалуйста, будет ли обратная связь по вопросам с других регионов или только по вопросам Ташкента?</p>
                            </div>
                        </div>
                    </article>
                    <ul className="children">
                        <li className="comment">
                            <article
                                className="comment-wrap clearfix">
                                <div
                                    className="gravatar">
                                    <img alt="image"
                                        src="assets/img/avatar.png" />
                                </div>
                                <div
                                    className="comment-content">
                                    <div
                                        className="comment-meta">
                                        <h6 className="comment-author">Андрей</h6>
                                        <span
                                            className="comment-time">Январ 22, 2021</span>
                                        <span
                                            className="comment-reply"><a
                                                className="comment-reply-link"
                                                href="#">Ответить</a></span>
                                    </div>
                                    <div
                                        className="comment-text">
                                        <p>Нашел для себя много интересного в этой платформе..Ответы от специалистов которым я задал вопрос очень пригодились в  жизни, огромное благодарность всем кто участвовал в создании !</p>
                                    </div>
                                </div>
                            </article>
                        </li>
                    </ul>
                </li>

                <li className="comment">
                    <article
                        className="comment-wrap clearfix">
                        <div className="gravatar"><img
                            alt="image"
                            src="assets/img/avatar.png" />
                        </div>
                        <div
                            className="comment-content">
                            <div
                                className="comment-meta">
                                <h6 className="comment-author">Бекзод</h6>
                                <span
                                    className="comment-time">Май 12, 2021</span>
                                <span
                                    className="comment-reply"><a
                                        className="comment-reply-link"
                                        href="#">Ответить</a></span>
                            </div>
                            <div
                                className="comment-text">
                                <p>Здравствуйте, а в будущем будут какие-то  семинары про психологию семьи и детей?</p>
                            </div>
                        </div>
                    </article>
                </li>
            </ol>

            <div id="respond"
                className="comment-respond">
                <h3 id="reply-title"
                    className="comment-reply-title">Оставить комментари</h3>
                <form action="#" method="post"
                    id="commentform"
                    className="comment-form">
                    <fieldset className="message-wrap">
                        <textarea id="comment-message" name="comment" rows="8" tabIndex="4"
                            placeholder="Сообщение"></textarea>
                    </fieldset>
                    <fieldset className="name-wrap">
                        <input type="text" id="author"
                            className="tb-my-input"
                            name="author"
                            tabIndex="1"
                            placeholder="Имя"
                            value="" size="32"
                            aria-required="true" />
                    </fieldset>
                    <fieldset className="email-wrap">
                        <input type="text" id="email"
                            className="tb-my-input"
                            name="email"
                            tabIndex="2"
                            placeholder="E-mail"
                            value="" size="32"
                            aria-required="true" />
                    </fieldset>
                    <p className="form-submit">
                        <input name="submit"
                            type="submit"
                            id="comment-reply"
                            className="submit"
                            value="Отправить" />
                        <input type="hidden"
                            name="comment_post_ID"
                            value="100"
                            id="comment_post_ID" />
                        <input type="hidden"
                            name="comment_parent"
                            id="comment_parent"
                            value="0" />
                    </p>
                </form>
            </div>
        </div>
    );
}

export default NewsSingleComments;
import FooterFotoPotok from "../Footer/FooterFotoPotok"

const NewsSingleSideBar = () => {

    return (

        <div className="col-md-3">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="sidebar">
                        <div id="inner-sidebar"
                            className="inner-content-wrap">
                            <div
                                className="widget widget_search margin-bottom-55">
                                <form role="search" method="get" action="#"
                                    className="search-form style-1">
                                    <input type="search"
                                        className="search-field"
                                        placeholder="Поиск" value=""
                                        name="s" title="Search for:" />
                                    <button type="submit"
                                        className="search-submit"
                                        title="Search">Поиск
                                    </button>
                                </form>
                            </div>

                            <div
                                className="widget widget_instagram margin-bottom-48">

                                <FooterFotoPotok />
                            </div>

                            <div className="widget widget_tag_cloud">
                                <h2 className="widget-title"><span>Популярные теги</span>
                                </h2>
                                <div className="tagcloud">
                                    <a href="/yearToOne">Семья</a>
                                    <a href="/credit">Кредиты</a>
                                    <a href="/yearToOne">Воспитание</a>
                                    <a href="/milkBaby">Дети</a>
                                    <a href="/facility">Льготы</a>
                                    <a href="/religion">Религия</a>
                                    <a href="/facility">Право</a>
                                    <a href="/psychology">Психология</a>
                                    <a href="/milkBaby">Консультация</a>
                                </div>
                            </div>


                            <div className="span_1_of_4 col">
                                <div className="widget widget_twitter margin-top-6 padding-left-13">
                                    <h2 className="widget-title"><span>Новости</span></h2>
                                    <ul className="tweets clearfix">
                                        <li className="item clearfix">
                                            <div className="tweet-icon">
                                                <img className="img" src="assets/img/sidebarnews/sidebar-news2.jpg"
                                                    alt="ImageFooter" style={{ height: '73px' }} />
                                            </div>

                                            <div className="texts">
                                                <h3><a href="/newsSingle" style={{ color: "black" }}>в феврале в республике родилось 166 пар близнецов.</a>
                                                </h3>

                                            </div>
                                        </li>
                                        <li className="item clearfix">
                                            <div className="tweet-icon">
                                                <img className="img" src="assets/img/sidebarnews/sidebar-news4.jpg"
                                                    alt="ImageFooter" style={{ height: '73px' }} />
                                            </div>

                                            <div className="texts">
                                                <h3><a href="/newsSingle" style={{ color: "black" }}>AKFA Medline будут роведены бесплатные консультации</a>
                                                </h3>

                                            </div>
                                        </li>
                                        <li className="item clearfix">
                                            <div className="tweet-icon">
                                                <img className="img" src="assets/img/sidebarnews/sidebar-news1.jpg"
                                                    alt="ImageFooter" style={{ height: "73px" }} />
                                            </div>

                                            <div className="texts">
                                                <h3><a href="/newsSingle" style={{ color: "black" }}>Remed Health приезжают в Ташкент</a>
                                                </h3>

                                            </div>
                                        </li>
                                        <li className="item clearfix">
                                            <div className="tweet-icon">
                                                <img className="img" src="assets/img/sidebarnews/sidebar-news3.jpg"
                                                    alt="ImageFooter" style={{ height: "73px" }} />
                                            </div>

                                            <div className="texts">
                                                <h3><a href="/newsSingle" style={{ color: "black" }}>Шавкат Мирзиёев приехал в махаллю «Ифтихор»</a>
                                                </h3>

                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NewsSingleSideBar;
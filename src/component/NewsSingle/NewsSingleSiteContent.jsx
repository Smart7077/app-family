import NewsSingleComments from "./NewsSingleComments"

const NewsSingleSiteContent = ({currentnews}) => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                         className="site-content clearfix">
                        <div id="inner-content"
                             className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src={`${currentnews.img}`}
                                            alt="Image"/></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="page-blog-single.html">{currentnews.content}</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">
                                                
                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div
                                            className="post-content post-excerpt">
                                            <p>{currentnews.description}</p>
                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="#" rel="tag">СЕМЬЯ</a>
                                            <a href="#" rel="tag">ЗДОРОВЬЕ</a>
                                            <a href="#"
                                                rel="tag">ПСИХОЛОГИЯ</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments />
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default NewsSingleSiteContent;
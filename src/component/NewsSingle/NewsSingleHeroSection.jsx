const NewsSingleHeroSection = (props) => {

    return (
        <div>

            <div className="hero-section slideshow text-center" data-height="840"
                data-count="2"
                data-image="./assets/img/slider/religinHero.jpg" data-effect="fade"
                data-overlay=""
                data-poverlay="">

                <div class="hero-content text-scroll">
                    <div className="themesflat-fancy-text scroll text-center">
                        <h1 className="heading font-weight-700 letter-spacing--2">Мы поможем вам</h1>
                        <h1 className="heading font-weight-700 letter-spacing--2">Сохранить очаг в</h1>
                        <h1 className="heading font-weight-700 letter-spacing--2">вашей новой жизни</h1>
                    </div>
                </div>
                <a class="arrow scroll-target" href="#scrolldown"></a>
            </div>

        </div>
    );
}

export default NewsSingleHeroSection;
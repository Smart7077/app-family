import React from 'react';

function News() {
    return (
        <div>
            <div id="wrapper" className="animsition">
                <div id="page" className="clearfix">
                    <div id="main-content" className="site-main clearfix">
                        <div id="content-wrap">
                            <div style={{ height: '100px', backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></div>
                            <div id="featured-title" className="featured-title clearfix">
                                <div id="featured-title-inner" className="themesflat-container clearfix">
                                    <div className="featured-title-inner-wrap">
                                        <div className="featured-title-heading-wrap">
                                            <h1 className="featured-title-heading">
                                                Новости
                                            </h1>
                                        </div>
                                        <div id="breadcrumbs">
                                            <div className="breadcrumbs-inner">
                                                <div className="breadcrumb-trail">
                                                    <a href="home.html" className="trail-begin">Home</a>
                                                    <span className="sep"><i className="finance-icon-chevron-right"></i></span>
                                                    <span className="trail-end"> Новости</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="site-content" className="site-content clearfix">
                                <div id="inner-content" className="inner-content-wrap">
                                    <div className="page-content">



                                        <div className="row-services bg-light-grey">
                                            <div className="container">
                                                <div className="row">

                                                    <div className="col-md-12">
                                                        <div className="themesflat-news "
                                                            
                                                            data-gap="35">
                                                            <div className="owl-carousel">
                                                                <div className="news-item style-1 clearfix">
                                                                    <div className="inner">
                                                                        <div className="thumb">
                                                                            <img src="assets/img/news/beby.png"
                                                                                alt="ImageNews" />
                                                                            <h3 className="title"><a
                                                                                href="/newsSingle"> В Узбекистане родилось 1166 пар близнецов.</a></h3>
                                                                        </div>
                                                                        <div className="text-wrap">

                                                                            <p className="excerpt-text">Госкомстат обнародовал
                                                                            статистику детей, родившихся в Узбекистане в феврале
                                                                2021 года.По информации Госкомстата....</p>

                                                                            <div className="post-btn">
                                                                                <a href="/newsSingle"
                                                                                    className="simple-link">ЧИТАТЬ</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="news-item style-1 clearfix">
                                                                    <div className="inner">
                                                                        <div className="thumb">
                                                                            <img src="assets/img/news/akfa.png"
                                                                                alt="ImageNews" />
                                                                            <h3 className="title"><a
                                                                                href="/newsSingle">будут проведены
                                                                бесплатные консультации</a></h3>
                                                                        </div>
                                                                        <div className="text-wrap">

                                                                            <p className="excerpt-text">Только 26 февраля и с 1 по 3
                                                                            марта в клинике AKFA Medline пройдут бесплатные
                                                                консультации зарубежных специалистов ....</p>

                                                                            <div className="post-btn">
                                                                                <a href="/newsSingle"
                                                                                    className="simple-link">ЧИТАТЬ</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="news-item style-1 clearfix">
                                                                    <div className="inner">
                                                                        <div className="thumb">
                                                                            <img src="assets/img/news/mart.png"
                                                                                alt="ImageNews" />
                                                                            <h3 className="title"><a
                                                                                href="/newsSingle">Remed Health
                                                                приезжают в Ташкент</a></h3>
                                                                        </div>
                                                                        <div className="text-wrap">

                                                                            <p className="excerpt-text">Ведущая компания Турции по
                                                                            медицинскому туризму Remed Health со своими самыми
                                                                опытными...</p>

                                                                            <div className="post-btn">
                                                                                <a href="/newsSingle"
                                                                                    className="simple-link">ЧИТАТЬ</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                   
                                                </div>
                                                <div style={{height: '60px'}}></div>
                                                <div className="row">

                                                    <div className="col-md-12">
                                                        <div className="themesflat-news "

                                                            data-gap="35">
                                                            <div className="owl-carousel">
                                                                <div className="news-item style-1 clearfix">
                                                                    <div className="inner">
                                                                        <div className="thumb">
                                                                            <img src="assets/img/news/president.png"
                                                                                alt="ImageNews" />
                                                                            <h3 className="title"><a
                                                                                href="/newsSingle">Мирзиёев
                                                                                приехал в махаллю
                                                                «Ифтихор»</a></h3>
                                                                        </div>
                                                                        <div className="text-wrap">

                                                                            <p className="excerpt-text">Во время беседы Шавкат Мирзиёев
                                                                            отметил, что начинается большая работа по созданию
                                                                необходимых условий...</p>

                                                                            <div className="post-btn">
                                                                                <a href="/newsSingle"
                                                                                    className="simple-link">ЧИТАТЬ</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="news-item style-1 clearfix">
                                                                    <div className="inner">
                                                                        <div className="thumb">
                                                                            <img src="assets/img/news/zamin.png"
                                                                                alt="ImageNews" />
                                                                            <h3 className="title"><a
                                                                                href="/newsSingle">Home market предлагает
                                                                выбор иподарков.</a></h3>
                                                                        </div>
                                                                        <div className="text-wrap">

                                                                            <p className="excerpt-text">Не можете выкроить время для
                                                                            предпраздничного шопинга? Не спешите, магазины Home
                                                                market работают...</p>

                                                                            <div className="post-btn">
                                                                                <a href="/newsSingle"
                                                                                    className="simple-link">ЧИТАТЬ</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="news-item style-1 clearfix">
                                                                    <div className="inner">
                                                                        <div className="thumb">
                                                                            <img src="assets/img/news/ziroat1.png"
                                                                                alt="ImageNews" />
                                                                            <h3 className="title"><a
                                                                                href="/newsSingle">Зироат Мирзиёева
                                                                                провела встречу с
                                                                    детьми</a></h3>
                                                                        </div>
                                                                        <div className="text-wrap">

                                                                            <p className="excerpt-text">В специализированной
                                                                            школе-интернате для глухих и слабослышащих детей № 123
                                                                Бухарской области открылся класс Zamin...</p>

                                                                            <div className="post-btn">
                                                                                <a href="/newsSingle"
                                                                                    className="simple-link">ЧИТАТЬ</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-12">
                                                        <div className="themesflat-spacer clearfix" data-desktop="90"
                                                            data-mobi="60" data-smobi="60"></div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default News;
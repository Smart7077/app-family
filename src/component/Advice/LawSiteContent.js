import NewsSingleComments from "../NewsSingle/NewsSingleComments"

const LawSiteContent = () => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                        className="site-content clearfix">
                        <div id="inner-content"
                            className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src="assets/img/sovet/Nikoh.jpg"
                                            alt="Image" /></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="page-blog-single.html">СОВЕТЫ</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">

                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="post-content post-excerpt">

                                            <p>
                                                Согласно Семейному кодексу брак не допускается в следующих случаях:
                                                <br />
                                                ● если один из супругов состоит в другом браке;
                                                <br />
                                                ● если супруги связаны родством (между внуками и бабушками и дедушками,
                                                между детьми и родителями);
                                                <br />
                                                ● если есть брак между братьями и сестрами;
                                                <br />
                                                ● при наличии брака между приемными родителями и приемными родителями;
                                                <br />
                                                ● Если один из супругов признан судом недееспособным по причине
                                                психического заболевания.</p>

                                            <h6>Вы знаете порядок брака?</h6>

                                            <p>Брак заключается в ЗАГСе.
Брак, заключенный по религиозным обрядам, не имеет юридического значения.
Брак заключается в личном присутствии супругов через месяц после подачи заявления в органы записи актов гражданского состояния. При наличии веских причин орган записи актов гражданского состояния может разрешить заключение брака до истечения одного месяца.</p>

                                            <p>В особых случаях (беременность, роды, болезнь одной из сторон и т. Д.) Брак может быть заключен в день подачи заявления.</p>

                                            <p>Брак добровольный.</p>

                                            <p>Брачный возраст для мужчин и женщин составляет 18 лет.</p>
                                            
                                            <h6>Какова ответственность за нарушение законодательства о брачном возрасте?</h6>

                                            <p>Согласно закону, возраст вступления в брак для мужчин и женщин составляет 18 лет.</p>

                                            <p>В исключительных случаях (беременность, роды, объявление несовершеннолетнего полностью годным для брака) мэр соответствующего района или города может снизить брачный возраст максимум на 1 год по просьбе желающих вступить в брак.</p>

                                            <p>Нарушение законодательства о брачном возрасте влечет административную ответственность.</p>
                                            
                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="#" rel="tag">СЕМЬЯ</a>
                                            <a href="#" rel="tag">РЕЛИГИЯ</a>
                                            <a href="#"
                                                rel="tag">ПСИХОЛОГИЯ</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments />
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default LawSiteContent;
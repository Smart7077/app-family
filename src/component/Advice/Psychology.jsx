import NewsSingleHeroSection from "../NewsSingle/NewsSingleHeroSection";
import NewsSingleFeaturedTitle from "../NewsSingle/NewsSingleFeaturedTitle";
import PsychologySiteContent from "./PsychologySiteContent";
import NewsSingleSideBar from "../NewsSingle/NewsSingleSideBar";


const Psychology = () => {

    return (
        <div>
            <div id="wrapper" className="animsition">
                <div id="page" className="clearfix">
                    <div id="content-wrap">
                        <div style={{ height: '100px', backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></div>
                        <NewsSingleFeaturedTitle pageName="СОВЕТЫ"
                            pageSection="СОВЕТЫ ПСИХОЛОГА" />
                        <div id="main-content" className="site-main clearfix">

                            <div id="site-content" className="site-content clearfix">
                                <div id="inner-content" className="inner-content-wrap">
                                    <div className="page-content">

                                        <div className="row-accordion">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="themesflat-spacer clearfix" data-desktop="91"
                                                            data-mobi="60" data-smobi="60"></div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <PsychologySiteContent />
                                                    <NewsSingleSideBar />
                                                    </div>

                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="themesflat-spacer clearfix" data-desktop="91"
                                                            data-mobi="60" data-smobi="60"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default Psychology;
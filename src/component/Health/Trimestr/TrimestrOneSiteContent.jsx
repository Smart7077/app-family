import NewsSingleComments from "../../NewsSingle/NewsSingleComments"

const TrimestrOneSiteContent = () => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                         className="site-content clearfix">
                        <div id="inner-content"
                             className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src="assets/img/health/trimestr1.jpg"
                                            alt="Image"/></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="/trimestrOne">1
                                                                    Триместр</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">

                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="post-content post-excerpt">
                                            <p>• Беременным необходим полноценный ночной сон. Бессонные ночи
                                                предрасполагают к нарушению тонуса кровеносных сосудов и, как следствие,
                                                к повышению (реже – понижению) артериального давления и нарушению
                                                кровоснабжения плаценты.<br/>! Регулярное отсутствие ночного сна
                                                (например, из-за работы в ночное время) на 50 % увеличивает риск
                                                преждевременных родов!</p>

                                            <p>• Во время беременности очень полезны длительные прогулки. Они хорошо
                                                влияют как на организм будущей мамы, так и на развитие плода: тренируют
                                                сердечно-сосудистую систему женщины, успокаивают нервную систему и
                                                улучшают снабжение плода кислородом. Если ваша беременность протекает
                                                нормально, то вам рекомендуется проводить на свежем воздухе 1–2 часа, из
                                                них 30–60 минут активно двигаться.<br/>
                                                При некоторых осложнениях беременности (отечность, повышенный тонус
                                                матки и др.) время и темп ходьбы следует уменьшать, однако пребывание на
                                                свежем воздухе даже более полезно, чем при нормально протекающей
                                                беременности.</p>

                                            <p>• Курение при беременности вредно. Никотин сужает сосуды плаценты, в
                                                результате чего наступает хроническая гипоксия плода. Помимо этого,
                                                никотин проникает в кровь плода и оказывает прямое вредное воздействие
                                                на его организм и прежде всего на формирующуюся нервную систему. В
                                                результате дети, рожденные курящими матерями, на 40 % чаще имеют
                                                различные психические отклонения и сложности с концентрацией
                                                внимания.<br/>

                                                ! Для беременных опасно и пассивное курение. Поэтому старайтесь избегать
                                                пребывания в накуренном помещении, в обществе курящих.
                                            </p>
                                            <h6>Ваше самочувствие</h6>
                                            <p>С вашим телом вскоре произойдут серьезные изменения, поскольку оно
                                                готовится к созданию новой жизни.<br/>

                                                У вас могут возникнуть такие симптомы, как тошнота или усталость, или,
                                                напротив, повыситься уровень энергии! Прислушивайтесь к своему телу и
                                                при необходимости корректируйте свой распорядок дня. Все женщины разные,
                                                и то же самое можно сказать о каждой беременности.</p>

                                            <h6>Ранние признаки и симптомы беременности</h6>
                                            <p>Самый ранний признак беременности — это задержка менструации у женщин с
                                                регулярным ежемесячным циклом. Иногда может возникать имплантационное
                                                кровотечение. Оно очень похоже на легкое менструальное кровотечение или
                                                мажущиеся кровянистые выделения. Это совершенно нормальное явление,
                                                однако при возникновении любого кровотечения во время беременности вам
                                                следует проконсультироваться со своим врачом.<br/>
                                                На ранних сроках беременности у вас также могут появиться некоторые из
                                                нижеперечисленных симптомов, такие как усталость, тошнота или учащенное
                                                мочеиспускание.</p>

                                            <h6>Общие симптомы</h6>
                                            <p>Гормональные изменения в первые недели беременности влияют на все ваше
                                                тело. Каждая беременность протекает индивидуально, однако в первом
                                                триместре у вас могут возникнуть некоторые из указанных симптомов:<br/>

                                                болезненная чувствительность груди
                                                резкие перепады настроения
                                                тошнота или рвота (утренняя тошнота)
                                                частое мочеиспускание
                                                набор или потеря веса
                                                сильная усталость
                                                головные боли
                                                изжога
                                                икроножные судороги
                                                поясничная и тазовая боль
                                                тяга к определенным продуктам питания
                                                возникновение отвращения к определенным продуктам питания
                                                запор</p>

                                            <h6>Самопомощь</h6>

                                            <p>Симптомы на ранних сроках беременности могут как минимум вызывать
                                                дискомфорт. Для того чтобы немного облегчить свое состояние, попробуйте
                                                следовать представленным здесь советам, предварительно
                                                проконсультировавшись со своим врачом. Помните, что при выборе тех или
                                                иных действий необходимо во всех случаях ориентироваться на собственные
                                                предпочтения и варианты, которые вам доступны.</p>

                                            <p>При тошноте или рвоте попробуйте принимать имбирь, ромашку или витамин B6
                                                и/или обратиться к специалистам по акупунктуре.
                                                При икроножных судорогах попробуйте принимать магний или кальций.
                                                При запоре в случае если изменения рациона питания, предложенные вашим
                                                врачом, не приносят желаемого результата, для облегчения своего
                                                состояния можно использовать пшеничные отруби или другие пищевые добавки
                                                с клетчаткой.</p>

                                            <p>Полезные для здоровья продукты питания и регулярные физические упражнения
                                                важны на всех этапах беременности. Сохраняйте ежедневную физическую
                                                активность до тех пор, пока вам будет удобно совершать те или иные
                                                действия. Чем активнее вы будете во время беременности, тем легче вам
                                                будет адаптироваться к изменениям своего тела.</p>

                                            <p>Позаботьтесь о полноценном питании для вашего растущего тела и растущего
                                                тела вашего ребенка. Следите за тем, чтобы вы получали достаточное
                                                количество энергии, белка, витаминов и минералов в результате
                                                употребления разнообразных полезных продуктов питания, в том числе
                                                овощей, мяса, бобов, орехов, пастеризованных молочных продуктов и
                                                фруктов.</p>
                                            <p></p>

                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="#" rel="tag">СЕМЬЯ</a>
                                            <a href="#" rel="tag">ЗДОРОВЬЕ</a>
                                            <a href="#"
                                                rel="tag">ПСИХОЛОГИЯ</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments />
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default TrimestrOneSiteContent;
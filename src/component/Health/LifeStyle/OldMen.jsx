import NewsSingleFeaturedTitle from "../../NewsSingle/NewsSingleFeaturedTitle";
import OldMenContent from "./OldMenContent";
import NewsSingleSideBar from "../../NewsSingle/NewsSingleSideBar";


const OldMen = () => {

    return (
        <div>
            <div id="wrapper" className="animsition">
                <div id="page" className="clearfix">
                    <div id="content-wrap">
                        <div style={{ height: '100px', backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></div>
                        <NewsSingleFeaturedTitle pageName="Здоровый образ жизни"
                            pageSection="Старения" />
                        <div id="main-content" className="site-main clearfix">

                            <div id="site-content" className="site-content clearfix">
                                <div id="inner-content" className="inner-content-wrap">
                                    <div className="page-content">

                                        <div className="row-accordion">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="themesflat-spacer clearfix" data-desktop="91"
                                                             data-mobi="60" data-smobi="60"></div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <OldMenContent/>
                                                    <NewsSingleSideBar/>
                                                </div>

                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="themesflat-spacer clearfix" data-desktop="91"
                                                             data-mobi="60" data-smobi="60"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default OldMen;
import NewsSingleComments from "../../NewsSingle/NewsSingleComments"

const VirusContent = () => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                         className="site-content clearfix">
                        <div id="inner-content"
                             className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src="assets/img/health/virus.jpg"
                                            alt="Image"/></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="page-blog-single.html">Коронавирус</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">

                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="post-content post-excerpt">
                                            <p>
                                                Что такое коронавирус?<br/>

                                                Коронавирус — это семейство вирусов, которые преимущественно поражают
                                                животных, но в некоторых случаях могут передаваться человеку. Обычно
                                                заболевания, вызванные коронавирусами, протекают в легкой форме, не
                                                вызывая тяжелой симптоматики.<br/>

                                                Как передается коронавирус?
                                                <br/>
                                                <br/>

                                                Как и другие респираторные вирусы, коронавирус распространяется через
                                                капли, которые образуются, когда инфицированный человек кашляет или
                                                чихает. Кроме того, в редких случаях он может распространяться, когда
                                                кто-то касается любой загрязненной поверхности, например, дверной ручки.
                                                Люди заражаются, когда они касаются загрязненными руками рта, носа или
                                                глаз.<br/>
                                                <br/>
                                                Каковы симптомы заболевания, вызванного новым коронавирусом?
                                                <br/>
                                                Чувство усталости
                                                Затруднённое дыхание
                                                Высокая температура
                                                Кашель и/или боль в горле
                                                <br/>
                                                Меры по профилактике коронавируса
                                                <br/>
                                                <br/>
                                                Самое важное, что можно сделать, чтобы защитить себя, — это соблюдать
                                                правила личной гигиены и сократить посещения общественных и людных мест.
                                                Держите руки в чистоте, часто мойте их водой с мылом или используйте
                                                дезинфицирующее средство. Старайтесь не касаться рта, носа или глаз
                                                немытыми руками (обычно такие прикосновения неосознанно свершаются нами
                                                в среднем 15 раз в час).
                                                <br/>
                                                <br/>

                                                На работе регулярно очищайте поверхности и устройства, к которым вы
                                                прикасаетесь (клавиатура компьютера, панели оргтехники общего
                                                использования, экран смартфона, пульты, дверные ручки и поручни).
                                                Носите с собой одноразовые салфетки и всегда прикрывайте нос и рот,
                                                когда вы кашляете или чихаете.
                                                <br/>
                                                <br/>
                                                Не ешьте еду из общих упаковок (орешки, чипсы, печенье и другие снеки),
                                                или посуды, если другие люди погружали в них свои пальцы.
                                                Объясните детям, как распространяются микробы и почему важна хорошая
                                                гигиена рук и лица. Расскажите детям о профилактике коронавируса. Часто
                                                проветривайте помещения.
                                                Если вы обнаружили симптомы, схожие с теми, которые вызывает
                                                коронавирус, оставайтесь дома и вызывайте врача.
                                                <br/>
                                                <br/>
                                                Почему опасно заниматься самолечением коронавируса?
                                                <br/>
                                                <br/>
                                                В отличие от привычных ОРВИ, ангины или гриппа, Covid-19 - новая
                                                инфекция, лечение от которой часто подбирается "на ощупь" даже врачами с
                                                многолетним опытом. Причудливая комбинация из противовирусных
                                                препаратов, антибиотиков или витаминов может помочь, а может и сделать
                                                хуже. Даже специалист не может быть до конца уверен в эффективности
                                                подобранной терапии, так как лекарств для лечения коронавируса на дому с
                                                научно подтвержденной эффективностью пока не существует.
                                                <br/>
                                                <br/>
                                                Какие лекарства назначать себе особенно опасно?
                                                Два самых распространенных варианта "врачебных рекомендаций" из
                                                протоколов лечения - это антибиотики и препараты, препятствующие
                                                образованию тромбов (антикоагулянты).
                                                <br/>
                                                <br/>
                                                Пример терапии, которую нельзя начинать слишком рано и тем более без
                                                назначения врача - прием антибиотиков. Согласно протоколам минздрава,
                                                они нужны только в случае присоединения к коронавирусу бактериальной
                                                инфекции. Назначать их "на всякий случай" нельзя, так как они ухудшают
                                                микрофлору организма, что, в свою очередь, может привести к снижению
                                                иммунитета.
                                            </p>
                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="#" rel="tag">СЕМЬЯ</a>
                                            <a href="#" rel="tag">ЗДОРОВЬЕ</a>
                                            <a href="#"
                                               rel="tag">ПСИХОЛОГИЯ</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments/>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default VirusContent;
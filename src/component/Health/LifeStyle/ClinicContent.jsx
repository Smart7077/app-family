import NewsSingleComments from "../../NewsSingle/NewsSingleComments"

const ClinicContent = () => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                         className="site-content clearfix">
                        <div id="inner-content"
                             className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src="assets/img/health/clinic.jpg"
                                            alt="Image"/></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="page-blog-single.html">Первая помощь</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">

                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="post-content post-excerpt">
                                            <p>
                                                Первая помощь — это ряд лечебно-профилактических мероприятий,
                                                выполняемых в необходимых при несчастных случаях и внезапных
                                                заболеваниях, меры срочной помощи раненым или больным людям,
                                                предпринимаемые до прибытия медработника или до помещения больного в
                                                медицинское учреждение.
                                                <br/>
                                                <br/>
                                                Порядок действий при оказании первой помощи
                                                Сначала нужно остановиться и внимательно убедиться в том, что опасность
                                                миновала. Если опасность присутствует, то следует либо устранить её,
                                                либо эвакуировать пострадавшего (не подвергая опасности себя), либо
                                                убежать (оповещая всех людей по пути об опасности). Такое поведение
                                                необходимо для соблюдения главного правила спасательных операций:
                                                количество пострадавших не должно увеличиться!
                                                <br/>
                                                <br/>
                                                В зависимости от ситуации могут быть проведены различные действия, в том
                                                числе возможны:
                                                <br/>
                                                <br/>
                                                -Вызов специалистов (в простейшем случае — 112 с мобильного телефона, со
                                                стационарного — скорая помощь — «103» или спасателей — «101»). Это
                                                является обязательным независимо от наличия навыков и возможности
                                                оказывать другие виды первой помощи.
                                                -Остановка кровотечения
                                                -Сердечно-лёгочная реанимация (СЛР)
                                                -Непрямой массаж сердца
                                                -Искусственное дыхание
                                                -Обеспечение физического и психологического комфорта пострадавшему.
                                                Предотвращение осложнений.
                                                <br/>
                                                <br/>
                                                Виды оказания помощи
                                                <br/>
                                                <br/>
                                                ПП→ПДП→ПВП→КМП→СМП.
                                                <br/>
                                                <br/>
                                                ПП — первая помощь (без использования специальных медицинских
                                                инструментов, оборудования, лекарств и проведения медицинских
                                                манипуляций, может оказывать любой человек)
                                                <br/>
                                                <br/>
                                                ПДП — первая доврачебная помощь (или Первичная доврачебная
                                                медико-санитарная помощь) (оказывает фельдшер, а также: медицинская
                                                сестра (брат), фармацевт, зубной врач, акушер, т.е. лицо, имеющее
                                                средне-специальное медицинское образование)
                                                <br/>
                                                <br/>
                                                ПВП — первая врачебная помощь (или Первичная врачебная медико-санитарная
                                                помощь) (оказывается врачом, имеющим необходимые инструментарий и
                                                лекарственные средства, по неотложным показаниям в рамках выполнения
                                                услуг по охране здоровья граждан при наличии соответствующих
                                                сертификатов. Оказывается вне больничных условий или в поликлинике,
                                                машине «скорой помощи», в приёмном отделении больницы)
                                                <br/>
                                                <br/>
                                                КМП — квалифицированная медицинская помощь (или Первичная
                                                специализированная медико-санитарная помощь) (оказывается
                                                врачами-специалистами высокой квалификации в условиях многопрофильных
                                                больниц, госпиталей, травматологических пунктов, специализированных
                                                врачебных бригад скорой медицинской помощи. Подразделяется на
                                                терапевтическую и хирургическую)
                                                <br/>
                                                <br/>
                                                СМП — специализированная медицинская помощь (или Специализированная, в
                                                том числе высокотехнологичная, медицинская помощь) (оказывается в
                                                условиях специализированных клиник, госпиталей, институтов и академий
                                                врачами-специалистами. К примеру: неврологическая, абдоминальная,
                                                офтальмологическая)
                                                <br/>
                                                <br/>
                                                При этом медицинские мероприятия по оказанию помощи не следует путать с
                                                лечением.
                                            </p>

                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="#" rel="tag">СЕМЬЯ</a>
                                            <a href="#" rel="tag">ЗДОРОВЬЕ</a>
                                            <a href="#"
                                               rel="tag">ПСИХОЛОГИЯ</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments/>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default ClinicContent;
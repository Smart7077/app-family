import React from 'react';
import HomeRequest from '../../Home/HomeRequest';


function HealthLife() {
    return (
        <div>
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap">
                    <div id="site-content" className="site-content clearfix">
                        <div id="inner-content" className="inner-content-wrap">

                            <div className="hero-section slideshow text-center" data-height="840" data-count="2"
                                 data-image="./assets/img/health/healthLife.jpg" data-effect="fade" data-overlay=""
                                 data-poverlay="">
                                <div className="hero-content">
                                    <div className="themesflat-fancy-text typed"
                                         data-fancy="нуждается в нашем уходе">
                                        <h1 className="heading font-weight-700 letter-spacing--2">Наше здоровье <span
                                            className="text"> нуждается в нашем уходе</span></h1>
                                    </div>

                                    <div className="themesflat-spacer clearfix" data-desktop="21" data-mobi="21"
                                         data-smobi="21"></div>
                                    <div className="elm-btn">
                                        <a href="#request" className="themesflat-button accent">Получить
                                            консультацию</a>
                                    </div>
                                </div>
                                <a className="arrow scroll-target" href="#content"></a>
                            </div>
                            <div id="content" className="row-services">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="themesflat-spacer clearfix" data-desktop="91" data-mobi="60"
                                                 data-smobi="60"></div>

                                            <div className="themesflat-headings style-1 clearfix text-center">
                                                <h2 className="heading clearfix">ЗДОРОВЫЙ ОБРАЗ ЖИЗНИ</h2>
                                                <div className="sep clearfix"></div>
                                                <p className="sub-heading clearfix">Здоровье – это состояние полного
                                                    физического, психического и социального благополучия, а не только
                                                    отсутствие болезней или физических факторов.
                                                    Здоровый образ жизни - образ жизни отдельного человека с целью
                                                    профилактики болезней и укрепления здоровья. ЗОЖ - это концепция
                                                    жизнедеятельности человека, направленная на улучшение и сохранение
                                                    здоровья с помощью соответствующего питания, физической подготовки,
                                                    морального настроя и отказа от вредных привычек.</p>
                                            </div>

                                            <div className="themesflat-spacer clearfix" data-desktop="52" data-mobi="40"
                                                 data-smobi="40"></div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-4 border-bottom-1 dark">
                                                <div
                                                    className="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-23">
                                                    <div className="icon-wrap">
                                                        <i>
                                                            <img src="/assets/icon/healthLife/virus.svg"/>
                                                        </i>
                                                    </div>

                                                    <h3 className="heading"><a href="/virus">Коронавирус</a></h3>

                                                    <p className="desc">Коронавирус — это семейство вирусов, которые
                                                        преимущественно поражают животных, но в некоторых случаях могут
                                                        передаваться человеку...</p>
                                                </div>
                                                <div className="themesflat-spacer clearfix" data-desktop="36"
                                                     data-mobi="40" data-smobi="40"></div>
                                            </div>

                                            <div className="col-md-4 border-left-1 border-right-1 border-bottom-1 dark">
                                                <div
                                                    className="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                    <div className="icon-wrap">
                                                        <i>
                                                            <img src="assets/icon/healthLife/hospital.svg"/>
                                                        </i>
                                                    </div>

                                                    <h3 class="heading"><a href="/clinic">Первая помощь</a></h3>

                                                    <p className="desc">Первая помощь — это ряд лечебно-профилактических
                                                        мероприятий,
                                                        выполняемых в необходимых при несчастных случаях и
                                                        ...</p>
                                                </div>
                                                <div className="themesflat-spacer clearfix" data-desktop="36"
                                                     data-mobi="40" data-smobi="40"></div>
                                            </div>

                                            <div className="col-md-4 border-bottom-1 dark">
                                                <div
                                                    className="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                    <div className="icon-wrap">
                                                        <i>
                                                            <img src="assets/icon/healthLife/tax-inspector.svg"/>
                                                        </i>
                                                    </div>

                                                    <h3 class="heading"><a href="/oldMen">Старение иммунитета</a></h3>

                                                    <p className="desc">С возрастом иммунная система человека становится
                                                        менее эффективной в борьбе с разными инфекциями и менее
                                                        восприимчивой к вакцинации…
                                                    </p>
                                                </div>
                                                <div className="themesflat-spacer clearfix" data-desktop="36"
                                                     data-mobi="0" data-smobi="0"></div>
                                            </div>
                                            <div className="col-md-4">
                                                <div className="themesflat-spacer clearfix" data-desktop="32"
                                                     data-mobi="40" data-smobi="40"></div>
                                                <div
                                                    className="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-23">
                                                    <div className="icon-wrap">
                                                        <i>
                                                            <img src="assets/icon/healthLife/liquid-soap.svg"/>
                                                        </i>
                                                    </div>

                                                    <h3 className="heading"><a href="/higienic">Гигиена</a></h3>

                                                    <p class="desc">Гигиена как наука представляет собой очень широкое
                                                        понятие, охватывающее практически все стороны жизни
                                                        людей.Слово «гигиена» произошло от греческого hygienos...</p>
                                                </div>
                                            </div>

                                            <div className="col-md-4 border-left-1 border-right-1 dark">
                                                <div className="themesflat-spacer clearfix" data-desktop="32"
                                                     data-mobi="40" data-smobi="40"></div>
                                                <div
                                                    className="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                    <div className="icon-wrap">
                                                        <i>
                                                            <img src="assets/icon/healthLife/fever.svg"/>
                                                        </i>
                                                    </div>

                                                    <h3 className="heading"><a href="/diseas">Наследственные
                                                        заболевания</a></h3>

                                                    <p className="desc">Наследственные заболевания - заболевания,
                                                        вызванные нарушением генетической информации Это в
                                                        основном вызвано...</p>
                                                </div>
                                            </div>

                                            <div className="col-md-4">
                                                <div className="themesflat-spacer clearfix" data-desktop="32"
                                                     data-mobi="40" data-smobi="40"></div>
                                                <div
                                                    class="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                    <div className="icon-wrap">
                                                        <i>
                                                            <img src="assets/icon/healthLife/diet.svg"/>
                                                        </i>
                                                    </div>

                                                    <h3 className="heading"><a href="/improperNutriton">Неправильное
                                                        питание</a></h3>

                                                    <p className="desc">Избыточный вес или ожирение может означать
                                                        другую форму переедания и определяется как избыточный вес для
                                                        этого роста...</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <HomeRequest id="request"/>
                            <div className="col-md-12">
                                <div className="themesflat-spacer clearfix" data-desktop="96" data-mobi="60"
                                     data-smobi="60"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}

export default HealthLife;

import React from 'react';
import HomeRequest from '../Home/HomeRequest';


function Baby() {
    return (
        <div>
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap">
                    <div id="site-content" className="site-content clearfix">
                        <div id="inner-content" className="inner-content-wrap">

                            <div className="hero-section slideshow text-center" data-height="840" data-count="2"
                                 data-image="./assets/img/bolalar/baby.jpg" data-effect="fade" data-overlay=""
                                 data-poverlay="">
                                <div className="hero-content">
                                    <div className="themesflat-fancy-text typed"
                                         data-fancy="нуждается в нашем уходе">
                                        <h1 className="heading font-weight-700 letter-spacing--2">Наше здоровье <span
                                            className="text"> нуждается в нашем уходе</span></h1>
                                    </div>

                                    <div className="themesflat-spacer clearfix" data-desktop="21" data-mobi="21"
                                         data-smobi="21"></div>
                                    <div className="elm-btn">
                                        <a href="#request" className="themesflat-button accent">Получить консультацию</a>
                                    </div>
                                </div>
                                <a className="arrow scroll-target" href="#content"></a>
                            </div>
                            <div id="content" className="row-services">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="themesflat-spacer clearfix" data-desktop="91" data-mobi="60"
                                                 data-smobi="60"></div>

                                            <div className="themesflat-headings style-1 clearfix text-center">
                                                <h2 className="heading clearfix">РЕБЕНОК</h2>
                                                <div className="sep clearfix"></div>
                                                <p className="sub-heading clearfix">Возрастной период, в котором
                                                    происходит половое созревание ребёнка и перестроение организма на
                                                    более высокий уровень развития — совершеннолетие</p>
                                            </div>

                                            <div className="themesflat-spacer clearfix" data-desktop="52" data-mobi="40"
                                                 data-smobi="40"></div>
                                        </div>

                                        <div className="col-md-4 border-right-1 dark padding-left-62 padding-right-45">
                                            <div
                                                className="themesflat-icon-box accent-bg clearfix w93 text-center rounded-100 has-width">
                                                <div className="icon-wrap">
                                                    <i>
                                                        <img style={{ width: '40px'}} src="./assets/icon/bebymilk.svg" />
                                                    </i>
                                                </div>

                                                <h3 className="heading font-size-16 font-weight-600 margin-top-17 margin-bottom-10">
                                                    <a href="/milkBaby">ГРУДНОЙ РЕБЕНОК</a></h3>

                                                <p className="desc">Грудной ребёнок — ребёнок в возрасте до одного года.
                                                    Различают период новорожденности (первые 4 недели после рождения) и
                                                    грудной возраст (от 4 недель до 1 года).Развитие грудного
                                                    ребёнка...</p>

                                                <div className="elm-btn">
                                                    <a className=" simple-link font-heading" href="/milkBaby">ЧИТАТЬ</a>
                                                </div>
                                            </div>

                                            <div className="themesflat-spacer clearfix" data-desktop="0" data-mobi="40"
                                                 data-smobi="40"></div>
                                        </div>
                                        <div className="col-md-4 border-right-1 dark padding-left-50 padding-right-45">
                                            <div
                                                className="themesflat-icon-box accent-bg clearfix w93 text-center rounded-100 has-width">
                                                <div className="icon-wrap">
                                                    <i>
                                                        <img style={{ width: '40px' }} src="./assets/icon/bebyschool.svg" />
                                                    </i>
                                                </div>

                                                <h3 className="heading font-size-16 font-weight-600 margin-top-17 margin-bottom-10">
                                                    <a href="/youngPupil">МЛАДШИЙ ШКОЛЬНИК</a></h3>

                                                <p className="desc">Ребёнок изучает работу пожарных в детском музее
                                                    Денвера.
                                                    На развитие личности младшего школьника имеет смысл посмотреть в
                                                    свете
                                                    формирования у него внутренней позиции, результатом ...</p>

                                                <div className="elm-btn">
                                                    <a className=" simple-link font-heading"
                                                       href="youngPupil">ЧИТАТЬ</a>
                                                </div>
                                            </div>

                                            <div className="themesflat-spacer clearfix" data-desktop="0" data-mobi="40"
                                                 data-smobi="40"></div>
                                        </div>

                                        <div className="col-md-4 padding-left-54 padding-right-45">
                                            <div
                                                className="themesflat-icon-box accent-bg clearfix w93 text-center rounded-100 has-width">
                                                <div className="icon-wrap">
                                                    <i>
                                                        <img style={{ width: '50px' }} src="./assets/icon/teenager.svg"/>
                                                    </i>
                                                </div>

                                                <h3 className="heading font-size-16 font-weight-600 margin-top-17 margin-bottom-10">
                                                    <a href="/teenagePeriod">ПОДРОСКОВЫЙ ПЕРИОД</a></h3>

                                                <p className="desc">Возрастной период, в котором происходит половое
                                                    созревание ребёнка и перестроение организма на более высокий уровень
                                                    развития — совершеннолетие.Современная наука ...</p>

                                                <div className="elm-btn">
                                                    <a className=" simple-link font-heading"
                                                       href="/teenagePeriod">ЧИТАТЬ</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <HomeRequest id="request"/>
                            <div className="col-md-12">
                                <div className="themesflat-spacer clearfix" data-desktop="96" data-mobi="60"
                                     data-smobi="60"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}

export default Baby;

import React from 'react';
import HomeRequest from '../../Home/HomeRequest';


function PeriodPregnancy() {
    return (
        <div>

            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap">
                    <div id="site-content" className="site-content clearfix">
                        <div id="inner-content" className="inner-content-wrap">

                            <div className="hero-section slideshow text-center" data-height="840" data-count="2"
                                 data-image="./assets/img/health/pregnancy1.jpg" data-effect="fade" data-overlay=""
                                 data-poverlay="">
                                <div className="hero-content">
                                    <div className="themesflat-fancy-text typed"
                                         data-fancy="маленького счастья..">
                                        <h1 className="heading font-weight-700 letter-spacing--2">Быть в
                                            предвкушении <span
                                                className="text"> маленького счастья..</span></h1>
                                    </div>

                                    <div className="themesflat-spacer clearfix" data-desktop="21" data-mobi="21"
                                         data-smobi="21"></div>
                                    <div className="elm-btn">
                                        <a href="#request" className="themesflat-button accent">Получить консультацию</a>
                                    </div>
                                </div>
                                <a className="arrow scroll-target" href="#content"></a>
                            </div>
                            <div id="content" className="row-services">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="themesflat-spacer clearfix" data-desktop="91" data-mobi="60"
                                                 data-smobi="60"></div>

                                            <div className="themesflat-headings style-1 clearfix text-center">
                                                <h2 className="heading clearfix">ПЕРИОД БЕРЕМЕННОСТИ</h2>
                                                <div className="sep clearfix"></div>
                                                <p className="sub-heading clearfix">Во время беременности очень полезны
                                                    длительные прогулки. Они хорошо
                                                    влияют как на организм будущей мамы, так и на развитие плода:
                                                    тренируют
                                                    сердечно-сосудистую систему женщины.</p>
                                            </div>

                                            <div className="themesflat-spacer clearfix" data-desktop="52" data-mobi="40"
                                                 data-smobi="40"></div>
                                        </div>

                                        <div className="col-md-4 border-right-1 dark padding-left-62 padding-right-45">
                                            <div
                                                className="themesflat-icon-box accent-bg clearfix w93 text-center rounded-100 has-width">
                                                <div className="icon-wrap">
                                                    <i>I</i>
                                                </div>

                                                <h3 className="heading font-size-16 font-weight-600 margin-top-17 margin-bottom-10">
                                                    <a href="/trimestrOne">1
                                                        ТРИМЕСТР</a></h3>

                                                <p className="desc">Беременным необходим полноценный ночной сон.
                                                    Бессонные ночи
                                                    предрасполагают к нарушению тонуса кровеносных сосудов и, как
                                                    следствие,
                                                    к повышению (реже – понижению).</p>

                                                <div className="elm-btn">
                                                    <a className=" simple-link font-heading"
                                                       href="/trimestrOne">ЧИТАТЬ</a>
                                                </div>
                                            </div>

                                            <div className="themesflat-spacer clearfix" data-desktop="0" data-mobi="40"
                                                 data-smobi="40"></div>
                                        </div>
                                        <div className="col-md-4 border-right-1 dark padding-left-50 padding-right-45">
                                            <div
                                                className="themesflat-icon-box accent-bg clearfix w93 text-center rounded-100 has-width">
                                                <div className="icon-wrap">
                                                    <i>II</i>
                                                </div>

                                                <h3 className="heading font-size-16 font-weight-600 margin-top-17 margin-bottom-10">
                                                    <a href="/trimestrTwo">2
                                                        ТРИМЕСТР</a></h3>

                                                <p className="desc">Беременным необходим полноценный ночной сон.
                                                    Бессонные ночи
                                                    предрасполагают к нарушению тонуса кровеносных сосудов и, как
                                                    следствие,
                                                    к повышению (реже – понижению).</p>

                                                <div className="elm-btn">
                                                    <a className=" simple-link font-heading"
                                                       href="/trimestrTwo">ЧИТАТЬ</a>
                                                </div>
                                            </div>

                                            <div className="themesflat-spacer clearfix" data-desktop="0" data-mobi="40"
                                                 data-smobi="40"></div>
                                        </div>

                                        <div className="col-md-4 padding-left-54 padding-right-45">
                                            <div
                                                className="themesflat-icon-box accent-bg clearfix w93 text-center rounded-100 has-width">
                                                <div className="icon-wrap">
                                                    <i>III</i>
                                                </div>

                                                <h3 className="heading font-size-16 font-weight-600 margin-top-17 margin-bottom-10">
                                                    <a href="/trimestrThree">3
                                                        ТРИМЕСТР</a></h3>

                                                <p className="desc">Поздравляем, вы вышли на финишную прямую! Скоро у
                                                    вас появится новый замечательный член семьи. В эти последние недели
                                                    вы можете ощущать бóльшую усталость и дискомфорт!</p>

                                                <div className="elm-btn">
                                                    <a className=" simple-link font-heading"
                                                       href="/trimestrThree">ЧИТАТЬ</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <HomeRequest id="request"/>
                            <div className="col-md-12">
                                <div className="themesflat-spacer clearfix" data-desktop="96" data-mobi="60"
                                     data-smobi="60"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    );
}

export default PeriodPregnancy;

import NewsSingleComments from "../../NewsSingle/NewsSingleComments"

const TeenagePeriodSiteContent = () => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                        className="site-content clearfix">
                        <div id="inner-content"
                            className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src="assets/img/health/podrostok.jpg"
                                            alt="Image" /></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="page-blog-single.html">Подростковый период</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">

                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                                                                </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="post-content post-excerpt">
                                            
                                            <p>Возрастной период, в котором происходит половое созревание ребёнка и перестроение организма на более высокий уровень развития — совершеннолетие.</p>

                                            <p>Подро́сток — юноша или девушка в переходном от детства к юности возрасте. Современная наука определяет подростковый возраст в зависимости от страны и культурно-национальных особенностей, а также пола (от 12—14 до 15—17 лет).</p>

                                            <p>Период формальных операций (11—15 лет)
                                            Основная способность, появляющаяся на стадии формальных операций — способность иметь дело с возможным, с гипотетическим, а внешнюю действительность воспринимать как частный случай того, что возможно могло бы быть. Реальность и собственные убеждения ребёнка перестают необходимым образом определять ход рассуждения.</p>

                                            <p>Ребёнок теперь смотрит на задачу не только с точки зрения непосредственно данного в ней, но прежде всего задаётся вопросом о всех возможных отношениях, в которых могут состоять, в которые могут быть включены элементы непосредственно данного.
</p>
                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="#" rel="tag">СЕМЬЯ</a>
                                            <a href="#" rel="tag">ЗДОРОВЬЕ</a>
                                            <a href="#"
                                                rel="tag">ПСИХОЛОГИЯ</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments />
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default TeenagePeriodSiteContent;
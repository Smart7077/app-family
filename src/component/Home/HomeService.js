import React from 'react';

function HomeService() {
    return (
        <div>
            <div className="row-services">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="92"
                                 data-mobi="60" data-smobi="60"></div>

                            <div
                                className="themesflat-headings style-1 clearfix text-center padding-left-32">
                                <h2 className="heading clearfix">Новости</h2>
                                <div className="sep clearfix"></div>

                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="50"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>

                        <div className="col-md-4">
                            <div className="themesflat-image-box style-1 clearfix">
                                <div className="item">
                                    <div className="inner">
                                        <div className="thumb"><img
                                            src="assets/img/imagebox/imagebox1.jpg"
                                            alt="ImageHome"/>
                                        </div>

                                        <div className="text-wrap">
                                            <h3 className="title">
                                                Почему важно знать свои права?</h3>

                                            <div className="desc">В первую очередь человеку необходимо знать свои права
                                                для того, чтобы иметь возможность их отстоять, опираясь на определенные
                                                законы и положения, ведь невозможно узнать, что права нарушены в случае
                                                их незнания. Также знание законов помогает не нарушать их самому.

                                                В повседневной жизни каждый человек должен знать и защищать свои права.

                                                Цель платформы – это оказание юридических услуг в сфере гражданских
                                                правоотношений, а также продвижение юридической грамотности в массы. На
                                                сайте можно получить юридическую консультацию онлайн или помощь в
                                                грамотном оформлении юридических документов
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="0"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>


                        <div className="col-md-4">
                            <div className="themesflat-image-box style-1 clearfix">
                                <div className="item">
                                    <div className="inner">
                                        <div className="thumb"><img
                                            src="assets/img/imagebox/imagebox2.jpg"
                                            alt="ImageHome"/>
                                        </div>

                                        <div className="text-wrap">
                                            <h3 className="title"><a target="_blank" href="/">PROJECT
                                                FINANCE</a></h3>

                                            <div className="desc">Create a sustainable financing
                                                structure
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="0"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>

                        <div className="col-md-4">
                            <div className="themesflat-image-box style-1 clearfix">
                                <div className="item">
                                    <div className="inner">
                                        <div className="thumb"><img
                                            src="assets/img/imagebox/imagebox3.jpg"
                                            alt="ImageHome"/>
                                        </div>

                                        <div className="text-wrap">
                                            <h3 className="title"><a target="_blank" href="/">REAL
                                                ESTATE PLANNING</a></h3>

                                            <div className="desc">How to finance your real
                                                estate investment
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="90"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>
                        <div className="themesflat-spacer clearfix" data-desktop="80" data-mobi="60"
                             data-smobi="60"></div>
                        <div className="border-h-1 dark"></div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomeService;
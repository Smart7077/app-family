import HomeTestimonial from "./HomeTestimonial";
import HomeSlider from "./HomeSlider";
import HomeVideo from "./HomeVideo";
import HomeFacts from "./HomeFacts";
import HomeRequest from "./HomeRequest";
import HomeNews from "./HomeNews";
import HomePlatform from "./HomePlatform";
import React, {useContext, useEffect, useState} from "react";
import {UserContext} from "../../util/UserContext";
import ServiceRowPartners from "../Service/ServiceRowPartners";
import TestimonialsModal from "./TestimonialsModal";
import '../../i18n'
import {useTranslation} from "react-i18next";
import axios from "axios";
import Map from "../Map";

function Home(props) {
    const msg = useContext(UserContext);
    const [tags, setTags] = useState([]);
    const lat = 59.95;
    const lng = 30.33;

    useEffect
    (() => {
        axios({
            method: 'get',
            url: "https://backend-family.napaautomotive.uz/api/posts",
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            }
        }).then(res => {
            console.log(res);
            console.log(res);
        })

    })
    const {t, i18} = useTranslation();
    const [selectedPlace, setSelectedPlace] = useState({
        name: 'Uzbekistan'
    });

    const onInfoWindowClose = () => {

    }

    const onMarkerClick = (props, marker, e) => {
        // ..
    }

    return (
        <div id="wrapper" className="animsition">
            <div id="page" className="clearfix">
                <div id="main-content" className="site-main clearfix">
                    <div id="content-wrap">
                        <div id="site-content" className="site-content clearfix">
                            <div id="inner-content" className="inner-content-wrap">
                                <div className="page-content">
                                    <HomeSlider/>
                                    <HomePlatform/>
                                    <HomeVideo/>
                                    <HomeNews/>
                                    <HomeFacts/>
                                    <HomeRequest/>
                                    <HomeTestimonial/>


                                    <div className="row-promotion bg-accent">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="themesflat-spacer clearfix" data-desktop="24"
                                                         data-mobi="40" data-smobi="40"></div>

                                                    <div
                                                        className="themesflat-action-box style-1 has-icon padding-left-110 padding-right-113">
                                                        <div className="inner">
                                                            <div className="heading-wrap">
                                                                <div className="text-wrap">
                                                                    <span className="icon "><img
                                                                        style={{width: '40px', marginBottom: '15px'}}
                                                                        src="assets/icon/chat.svg"/></span>
                                                                    <h3 className="heading mobi-padding-top20 mobi-padding-bottom20">
                                                                        Оставьте своё мнение о нашей платформе и
                                                                        поделитесь в социальных сетях
                                                                    </h3>
                                                                    <span className="icon"><i
                                                                        class="as-icon-speedometer2"></i></span>
                                                                </div>
                                                            </div>
                                                            <div className="button-wrap">
                                                                <TestimonialsModal
                                                                    className="themesflat-button white font-weight-600 margin-top-10 margin-bottom-13"
                                                                    text="Оставить отзыв"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="themesflat-spacer clearfix" data-desktop="20"
                                                         data-mobi="40" data-smobi="40"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>


                                <div className="row-maps">
                                    <div className="container">
                                        <div className="row clearfix">
                                            <div className="col-md-12">
                                                <div className="themesflat-spacer clearfix" data-desktop="94"
                                                     data-mobi="60"
                                                     data-smobi="60"></div>

                                                {/*<div className="themesflat-gmap style-1" data-lat="41.320151"*/}
                                                {/*     data-lng="69.262558" data-marker="assets/img/marker.png"*/}
                                                {/*     data-zoom="14"></div>*/}


                                                <div className="container" style={{height: '60vh', width: '90%'}}>
                                                    <Map/>
                                                    {/*<GoogleMapReact*/}
                                                    {/*    defaultZoom={12}*/}
                                                    {/*    defaultCenter={{lat: 41.320151, lng: 69.262558}}*/}
                                                    {/*>*/}
                                                    {/*    <Marker*/}
                                                    {/*        key={1}*/}
                                                    {/*        position={{*/}
                                                    {/*            lat: 41.320151,*/}
                                                    {/*            lng: 69.262558*/}
                                                    {/*        }}*/}

                                                    {/*    />*/}
                                                    {/*</GoogleMapReact>*/}
                                                </div>
                                                <div className="themesflat-spacer clearfix" data-desktop="94"
                                                     data-mobi="60"
                                                     data-smobi="60"></div>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {/*<div className="container"  style={{height: '60vh', width: '90%'}}>*/}
                                {/*    */}
                                {/*    <GoogleMapReact*/}
                                {/*        defaultCenter={center}*/}
                                {/*        defaultZoom={zoom}*/}
                                {/*    >*/}
                                {/*        <AnyReactComponent text={'salom'}*/}
                                {/*            lat={41.320151}*/}
                                {/*            lng={69.262558}*/}
                                {/*            text="My Marker"*/}
                                {/*        />*/}
                                {/*    </GoogleMapReact>*/}
                                {/*</div>*/}

                                {/*<div className="themesflat-spacer clearfix" data-desktop="94"*/}
                                {/*     data-mobi="60"*/}
                                {/*     data-smobi="60"></div>*/}


                                <ServiceRowPartners/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home;

// export default GoogleApiWrapper({
//     apiKey: ("AIzaSyDhYZ0AImn2EBDty7kCAHWDWbAsZ7JtL-M")
// })(Home)
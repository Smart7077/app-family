import React from 'react';
import {Link} from "react-router-dom";

function HomeNews() {


    const writeId = (id) => {
        localStorage.setItem('newsId', id);
    };
    return (
        <div>

            <div id="site-content" className="site-content clearfix">
                <div id="inner-content" className="inner-content-wrap">
                    <div className="page-content">

                        <div className="row-services">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="themesflat-spacer clearfix" data-desktop="90"
                                             data-mobi="60" data-smobi="60"></div>
                                    </div>
                                    <div className="col-md-12">
                                        <div
                                            className="themesflat-news has-arrows w32 arrows-circle arrow-top arrow30"
                                            data-auto="false" data-column="3" data-column2="2"
                                            data-column3="1"
                                            data-gap="30">
                                            <div className="owl-carousel owl-theme">
                                                <div className="news-item clearfix">
                                                    <div className="inner">
                                                        <div className="thumb">
                                                            <img src="assets/img/news/beby.png"
                                                                 alt="ImageNews"/>
                                                            <div className="meta">
                                                                                <span
                                                                                    className="post-events">СОБЫТИЕ</span>
                                                            </div>
                                                        </div>
                                                        <div className="text-wrap">
                                                            <h3 className="title"><a
                                                                href="/newsSingle" onClick={() => writeId(0)}>
                                                                в феврале в республике родилось 1166 пар близнецов.</a></h3>

                                                            <p className="excerpt-text">Госкомстат обнародовал
                                                                статистику детей, родившихся в Узбекистане в феврале
                                                                2021 года.По информации Госкомстата....</p>

                                                            <div className="post-btn">
                                                                <a href="/newsSingle" onClick={() => writeId(0)}
                                                                   className="small themesflat-button outline ol-accent">ЧИТАТЬ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="news-item clearfix">
                                                    <div className="inner">
                                                        <div className="thumb">
                                                            <img src="assets/img/news/akfa.png"
                                                                 alt="ImageNews"/>
                                                            <div className="meta">
                                                                                <span
                                                                                    className="post-date">СОБЫТИЕ</span>
                                                            </div>
                                                        </div>
                                                        <div className="text-wrap">
                                                            <h3 className="title"><a onClick={() => writeId(1)}
                                                                                     href="/newsSingle">AKFA Medline
                                                                будут проведены
                                                                бесплатные консультации</a></h3>

                                                            <p className="excerpt-text">Только 26 февраля и с 1 по 3
                                                                марта в клинике AKFA Medline пройдут бесплатные
                                                                консультации зарубежных специалистов ....</p>

                                                            <div className="post-btn">
                                                                <a href="/newsSingle" onClick={() => writeId(1)}
                                                                   className="small themesflat-button outline ol-accent">ЧИТАТЬ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="news-item clearfix">
                                                    <div className="inner">
                                                        <div className="thumb">
                                                            <img src="assets/img/news/mart.png"
                                                                 alt="ImageNews"/>
                                                            <div className="meta">
                                                                                <span
                                                                                    className="post-date">СОБЫТИЕ</span>
                                                            </div>
                                                        </div>
                                                        <div className="text-wrap">
                                                            <h3 className="title"><a onClick={() => writeId(2)}
                                                                                     href="/newsSingle">Remed Health
                                                                приезжают в Ташкент</a>
                                                            </h3>

                                                            <p className="excerpt-text">Ведущая компания Турции по
                                                                медицинскому туризму Remed Health со своими самыми
                                                                опытными...</p>

                                                            <div className="post-btn">
                                                                <a href="/newsSingle" onClick={() => writeId(2)}
                                                                   className="small themesflat-button outline ol-accent">ЧИТАТЬ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="news-item clearfix">
                                                    <div className="inner">
                                                        <div className="thumb">
                                                            <img src="assets/img/news/president.png"
                                                                 alt="ImageNews"/>
                                                            <div className="meta">
                                                                                <span
                                                                                    className="post-events">СОБЫТИЕ</span>
                                                            </div>
                                                        </div>
                                                        <div className="text-wrap">
                                                            <h3 className="title"><a onClick={() => writeId(3)}
                                                                                     href="/newsSingle">Шавкат Мирзиёев
                                                                приехал в махаллю
                                                                «Ифтихор»</a></h3>

                                                            <p className="excerpt-text">Во время беседы Шавкат Мирзиёев
                                                                отметил, что начинается большая работа по созданию
                                                                необходимых условий...</p>

                                                            <div className="post-btn">
                                                                <a href="/newsSingle" onClick={() => writeId(3)}
                                                                   className="small themesflat-button outline ol-accent">ЧИТАТЬ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="news-item clearfix">
                                                    <div className="inner">
                                                        <div className="thumb">
                                                            <img src="assets/img/news/zamin.png"
                                                                 alt="ImageNews"/>
                                                            <div className="meta">
                                                                                <span
                                                                                    className="post-date">СОБЫТИЕ</span>
                                                            </div>
                                                        </div>
                                                        <div className="text-wrap">
                                                            <h3 className="title"><a onClick={() => writeId(4)}
                                                                                     href="/newsSingle">Сеть магазинов
                                                                Home market предлагает
                                                                выбор иподарков.</a></h3>

                                                            <p className="excerpt-text">Не можете выкроить время для
                                                                предпраздничного шопинга? Не спешите, магазины Home
                                                                market работают...</p>

                                                            <div className="post-btn">
                                                                <a href="/newsSingle" onClick={() => writeId(4)}
                                                                   className="small themesflat-button outline ol-accent">ЧИТАТЬ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="news-item clearfix">
                                                    <div className="inner">
                                                        <div className="thumb">
                                                            <img src="assets/img/news/fond.png"
                                                                 alt="ImageNews"/>
                                                            <div className="meta">
                                                                                <span
                                                                                    className="post-date">СОБЫТИЕ</span>
                                                            </div>
                                                        </div>
                                                        <div className="text-wrap">
                                                            <h3 className="title"><a onClick={() => writeId(5)}
                                                                                         href="/newsSingle">Зироат Мирзиёева
                                                                    провела встречу с
                                                                    детьми</a></h3>

                                                            <p className="excerpt-text">В специализированной
                                                                школе-интернате для глухих и слабослышащих детей № 123
                                                                Бухарской области открылся класс Zamin...</p>

                                                            <div className="post-btn">
                                                                <a href="/newsSingle" onClick={() => writeId(5)}
                                                                   className="small themesflat-button outline ol-accent">ЧИТАТЬ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/*<div className="col-md-12">*/}
                                    {/*    <div className="themesflat-spacer clearfix" data-desktop="92"*/}
                                    {/*         data-mobi="60" data-smobi="60"></div>*/}
                                    {/*</div>*/}
                                    <div className="themesflat-spacer clearfix" data-desktop="80" data-mobi="60"
                                         data-smobi="60"></div>
                                    <div className="border-h-1 dark"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default HomeNews;
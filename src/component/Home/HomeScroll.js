import React from 'react';
import {Link} from "react-scroll";


function HomeScroll() {


    return (
        <div>
            <div id="wrapper" className="animsition">
                <div id="page" className="clearfix">
                    <div id="site-header-wrap" className="absolute">
                        <header id="site-header">
                            <div className="wrap-themesflat-container">
                                <div id="site-header-inner" className="themesflat-container">
                                    <div className="wrap-inner">
                                        <Link
                                            activeClass="active"
                                            to="/"
                                            spy={true}
                                            smooth={true}
                                            offset={0}
                                            duration={500}
                                        >Home</Link>

                                        <Link
                                            activeClass="active"
                                            to="/contact"
                                            spy={true}
                                            smooth={true}
                                            offset={0}
                                            duration={500}
                                        >Contact</Link>

                                        <Link
                                            activeClass="active"
                                            to="/about"
                                            spy={true}
                                            smooth={true}
                                            offset={0}
                                            duration={500}
                                        >About</Link>
                                    </div>
                                </div>
                            </div>
                        </header>
                    </div>
                </div>
            </div>


            {/*<Link*/}
            {/*    activeClass="active"*/}
            {/*    to="/"*/}
            {/*    spy={true}*/}
            {/*    smooth={true}*/}
            {/*    offset={0}*/}
            {/*    duration={500}*/}
            {/*>Home</Link>*/}

            {/*<Link*/}
            {/*    activeClass="active"*/}
            {/*    to="/contact"*/}
            {/*    spy={true}*/}
            {/*    smooth={true}*/}
            {/*    offset={0}*/}
            {/*    duration={500}*/}
            {/*>Contact</Link>*/}

            {/*<Link*/}
            {/*    activeClass="active"*/}
            {/*    to="/about"*/}
            {/*    spy={true}*/}
            {/*    smooth={true}*/}
            {/*    offset={0}*/}
            {/*    duration={500}*/}
            {/*>About</Link>*/}

        </div>
    );
}

export default HomeScroll;
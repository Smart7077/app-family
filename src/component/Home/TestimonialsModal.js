import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export default function TestimonialsModal(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <button type="button" onClick={handleOpen}>{props.text}</button>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div style={{height: '340px', width: '500px', backgroundColor: 'white', padding: '20px', borderRadius: '10px'}}>
                        <div class="themesflat-contact-form style-1 color-light-grey">
                            <form action="http://themesflat.com/html/ficonsu/includes/contact/contact-process.php" method="post" class="contact-form wpcf7-form">
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" tabindex="1" id="name" name="name" value="" class="wpcf7-form-control" placeholder="Имя*" required />
                                </span>
                                <span class="wpcf7-form-control-wrap your-phone">
                                    <input type="text" tabindex="2" id="phone" name="phone" value="" class="wpcf7-form-control" placeholder="Номер телефона*" required />
                                </span>
                                <span class="wpcf7-form-control-wrap your-select">
                                    <select>
                                        <option value="">Оцените платформу &#9733; &#9733; &#9733;</option>
                                        <option value="">Оцените платформу &#9733; &#9733; &#9733; &#9733;</option>
                                        <option value="">Оцените платформу &#9733; &#9733; &#9733; &#9733; &#9733;</option>
                                    </select>
                                </span>
                                
                                <span class="wpcf7-form-control-wrap your-message">
                                    <textarea name="message" tabindex="4" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" placeholder="Ваш отзивь*" required></textarea>
                                </span>
                                <div class="wrap-submit">
                                    <input type="submit" value="Отправить" class="submit wpcf7-form-control wpcf7-submit" id="submit" name="submit" />
                                </div>
                            </form>
                        </div>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
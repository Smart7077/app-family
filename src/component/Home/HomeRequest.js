import React from 'react';

function HomeRequest() {
    return (
        <div id="request">
            <div className="themesflat-spacer clearfix" data-desktop="90"
                data-mobi="40" data-smobi="40"></div>
            <div className="row-request">
                <div className="container-fluid">
                    <div className="container">
                        <div className="row ">
                            <div className="col-md-6 bg-request ">

                            </div>

                            <div className="col-md-6 bg-light-grey" >
                                <div className="themesflat-spacer clearfix" data-desktop="85"
                                    data-mobi="40" data-smobi="40"></div>

                                <div className="themesflat-content-box clearfix "
                                    data-margin="0% 10.8% 0% 10.7%" data-mobimargin="0% 5% 0% 5%">
                                    <div className="themesflat-headings style-3 clearfix">
                                        <h2 className="heading clearfix">Получить консультацию от специалиста</h2>
                                        <div className="sep clearfix"></div>
                                        <p className="sub-heading clearfix">Пожалуйста, заполните анкету. Наши эксперти свяжутся
                                        с вами.</p>
                                    </div>

                                    <div className="themesflat-spacer clearfix" data-desktop="32"
                                        data-mobi="40" data-smobi="40"></div>

                                    <div className="themesflat-contact-form style-1">
                                        <form
                                            action="http://themesflat.com/html/ficonsu/includes/contact/contact-process.php"
                                            method="post" className="contact-form wpcf7-form">
                                            <span className="wpcf7-form-control-wrap your-name">
                                                <input type="text" tabIndex="1" id="name" name="name"
                                                    className="wpcf7-form-control" placeholder="Имя*"
                                                    required />
                                            </span>
                                            <span className="wpcf7-form-control-wrap your-phone">
                                                <input type="text" tabIndex="2" id="phone" name="phone"
                                                    className="wpcf7-form-control" placeholder="Ваш номер *"
                                                    required />
                                            </span>
                                            <span className="wpcf7-form-control-wrap your-select">
                                                <select>
                                                    <option>Удобное для вас время</option>
                                                    <option>Удобное для вас время</option>
                                                    <option>Удобное для вас время</option>
                                                </select>
                                            </span>
                                            <span className="wpcf7-form-control-wrap your-subject">
                                                <input type="text" tabIndex="3" id="subject" name="subject"
                                                    className="wpcf7-form-control"
                                                    placeholder="Тема*" required />
                                            </span>
                                            <span className="wpcf7-form-control-wrap your-message">
                                                <textarea name="message" tabIndex="4" cols="40" rows="10"
                                                    className="wpcf7-form-control wpcf7-textarea"
                                                    placeholder="Ваше сообщение*" required />
                                            </span>
                                            <div className="wrap-submit">
                                                <input type="submit" style={{ color: 'white' }} value="ОТПРАВИТЬ"
                                                    className="submit  text-uppercase wpcf7-form-control wpcf7-submit"
                                                    id="submit" name="submit" />
                                            </div>
                                            {/*<span className="your-notification">or write us on <a*/}
                                            {/*    href="/">Get in Touch</a> page.</span>*/}
                                        </form>
                                    </div>
                                </div>

                                <div className="themesflat-spacer clearfix" data-desktop="90"
                                    data-mobi="40" data-smobi="40"></div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomeRequest;
import React from 'react';

function HomePromotion() {
    return (
        <div>
            <div className="row-promotion bg-accent">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="24"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-action-box style-1 has-icon padding-left-106 padding-right-113">
                                <div className="inner">
                                    <div className="heading-wrap">
                                        <div className="text-wrap">
                                            <span className="icon finance-icon-award"></span>
                                            <h3 className="heading mobi-padding-top20 mobi-padding-bottom20">
                                                PROFESSIONAL HELP IN PLANNING YOUR FINANCIAL
                                                FUTURE
                                            </h3>
                                            <span className="icon"><i
                                                className="as-icon-speedometer2"></i></span>
                                        </div>
                                    </div>
                                    <div className="button-wrap">
                                        <a href="/"
                                           className="themesflat-button white font-weight-600 margin-top-10 margin-bottom-13">CONTACT
                                            US TODAY</a>
                                    </div>
                                </div>
                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="20"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    );
}

export default HomePromotion;
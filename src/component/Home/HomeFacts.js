import React from 'react';

function HomeFacts() {
    return (
        <div>
            <div className="row-facts-1 parallax parallax-overlay">
                <div className="bg-parallax-overlay overlay-black"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="104"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>

                        <div className="col-md-3">
                            <div className="themesflat-animation-block"
                                 data-animate="fadeInUpSmall" data-duration="1s" data-delay="0"
                                 data-position="80%">
                                <div
                                    className="themesflat-counter style-3 mobi-center clearfix icon-top margin-top--27">
                                    <div className="inner">
                                        <div className="text-wrap">
                                            <div className="number-wrap font-heading">
                                                                        <span
                                                                            className="prefix">Статистика на<br/></span><span
                                                className="number accent" data-speed="3000"
                                                data-to="2020" data-inviewport="yes">2020</span><span
                                                className="suffix">-год</span>
                                            </div>

                                            <div className="sep"></div>
                                            <h3 className="heading">Государственный комитет статистики</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="0"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>


                        <div className="col-md-3">
                            <div className="themesflat-animation-block"
                                 data-animate="fadeInUpSmall" data-duration="1s"
                                 data-delay="0.15s" data-position="80%">
                                <div
                                    className="themesflat-counter style-2 clearfix icon-top text-center">
                                    <div className="inner">
                                        <div className="text-wrap">
                                            <div className="number-wrap font-heading">
                                                <span className="prefix"></span><span
                                                className="number accent" data-speed="3000"
                                                data-to="296800" data-inviewport="yes">296 800 </span><span
                                                className="suffix">+</span>
                                            </div>

                                            <div className="sep"></div>
                                            <h3 className="heading text-uppercase">Nikoh qayt etildi</h3>
                                        </div>

                                        <div className="border-right-2"></div>
                                    </div>
                                </div>

                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="0"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>


                        <div className="col-md-3">
                            <div className="themesflat-animation-block"
                                 data-animate="fadeInUpSmall" data-duration="1s"
                                 data-delay="0.3s" data-position="80%">
                                <div
                                    className="themesflat-counter style-2 clearfix icon-top text-center">
                                    <div className="inner">
                                        <div className="text-wrap">
                                            <div className="number-wrap font-heading">
                                                <span className="prefix"></span><span
                                                    className="number accent" data-speed="3000"
                                                    data-to="841000" data-inviewport="yes">841 000</span><span
                                                        className="suffix">+</span>
                                            </div>

                                            <div className="sep"></div>
                                            <h3 className="heading text-uppercase">Bola dunyoga keldi</h3>
                                        </div>
                                        
                                    </div>
                                </div>

                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="0"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>


                        <div className="col-md-3">
                            <div className="themesflat-animation-block"
                                 data-animate="fadeInUpSmall" data-duration="1s"
                                 data-delay="0.45s" data-position="80%">
                                <div
                                    className="themesflat-counter style-2 clearfix icon-top text-center">
                                    <div className="inner">
                                        <div className="text-wrap">
                                            <div className="number-wrap font-heading">
                                                <span className="prefix"></span><span
                                                    className="number accent" data-speed="3000"
                                                    data-to="268600" data-inviewport="yes">268 600</span><span
                                                        className="suffix">+</span>
                                            </div>

                                            <div className="sep"></div>
                                            <h3 className="heading text-uppercase">Baxtiyor oila</h3>
                                        </div>
                                        
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="72"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomeFacts;
import React from 'react';

function HomeServiceList() {
    return (
        <div>
            <div className="row-services">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="91"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-headings style-2 clearfix text-center padding-left-24">
                                <h2 className="heading clearfix">OUR ANALYSIS COVERS</h2>
                                <p className="sub-heading clearfix">We have knowledgeable
                                    practitioners including a Certified Financial Planner, a
                                    Certified Investment Management Analyst, an attorney, and a
                                    tax practitioner.</p>
                                <div className="sep clearfix"></div>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="60"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>


                        <div className="col-md-4">
                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-bank-2"></i>
                                </div>

                                <h3 className="heading"><a href="/">LOAN INVESTMENT</a></h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="54"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-chat"></i>
                                </div>

                                <h3 className="heading"><a href="/">FINANCIAL CONSULTING</a>
                                </h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="52"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-hammer-2"></i>
                                </div>

                                <h3 className="heading"><a href="/">INSURANCE CONSULTING</a>
                                </h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="0"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>


                        <div className="col-md-4">
                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-signature-1"></i>
                                </div>

                                <h3 className="heading"><a href="/">EDUCATION PLANNING</a></h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="54"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-graphic"></i>
                                </div>

                                <h3 className="heading"><a href="/">INVESTMENT MANAGEMENT</a>
                                </h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="52"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-security"></i>
                                </div>

                                <h3 className="heading"><a href="/">PROTECTION PLANNING</a></h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="0"
                                 data-mobi="40" data-smobi="40"></div>
                        </div>


                        <div className="col-md-4">
                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-safebox-3"></i>
                                </div>

                                <h3 className="heading"><a href="/">RETIREMENT & INCOME</a></h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="54"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-buildings-2"></i>
                                </div>

                                <h3 className="heading"><a href="/">ESTATE PLANNING</a></h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="52"
                                 data-mobi="40" data-smobi="40"></div>

                            <div
                                className="themesflat-icon-box style-3 clearfix icon-left w70 dark-bg align-left rounded-100 has-width">
                                <div className="icon-wrap">
                                    <i className="finance-icon-cheque-2"></i>
                                </div>

                                <h3 className="heading"><a href="/">TAXES PREPARATION</a></h3>

                                <p className="desc">We've devoted our careers to providing the
                                    holistic wealth management that helps clients reach their
                                    financial goals.</p>
                            </div>

                        </div>


                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="107"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    );
}

export default HomeServiceList;
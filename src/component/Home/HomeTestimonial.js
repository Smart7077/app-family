import React from 'react';

function HomeTestimonial() {
    return (
        <div>
            <div className="row-testimonials">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="themesflat-spacer clearfix" data-desktop="93"
                                 data-mobi="40" data-smobi="40"></div>

                            <div className="themesflat-headings style-1 clearfix text-center">
                                <h2 className="heading clearfix">Счастливые семьи про нас</h2>
                                <div className="sep clearfix"></div>
                                <p className="sub-heading clearfix">Ваш отзыв для нас неоценим для дальнейшего развития
                                    и процветания нашей платформы!</p>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="52"
                                 data-mobi="40" data-smobi="40"></div>

                            <div className="themesflat-carousel-box has-bullets bullet-circle"
                                 data-auto="true" data-loop="false" data-gap="25"
                                 data-column="3" data-column2="2" data-column3="1">
                                <div className="owl-carousel owl-theme">
                                    <div
                                        className="themesflat-testimonials style-1 clearfix image-circle">
                                        <div className="item">
                                            <div className="inner">
                                                <div className="thumb">
                                                    <img
                                                        src="assets/img/testimonials/otziv4.png"
                                                        alt="ImageHome"/>
                                                </div>
                                                <blockquote className="text" style={{height: 350}}>
                                                    <div className="start">
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                    </div>
                                                    <p>Нашел для себя много интересного в этой платформе..Ответы от
                                                        специалистов которым я задал вопрос очень пригодились в жизни,
                                                        огромное благодарность всем кто участвовал в создании !Спасиба за такое платформу.</p>
                                                    <div className="name-pos">
                                                        <h6 className="name">Yulduz Ergasheva</h6>
                                                        {/*<span className="position">Delicates Studio</span>*/}
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        className="themesflat-testimonials style-1 clearfix image-circle">
                                        <div className="item">
                                            <div className="inner">
                                                <div className="thumb">
                                                    <img
                                                        src="assets/img/testimonials/otziv2.png"
                                                        alt="ImageHome"/>
                                                </div>

                                                <blockquote className="text" style={{height: 350}}>
                                                    <div className="start">
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                    </div>
                                                    <p>
                                                        Ayolim bilan yaqinda oila qurgan edik , moliyaviy jihatdan biroz
                                                        qiynaldik bu platforma orqali ilgari bilmagan yosh oilalarga
                                                        beriladigan kredit va lgotalarni bilib oldim.Kirishim bilan yordamchi savol qabul qilib oldi ,hamma taraflama
                                                        spetsialistlar bor ekan...</p>
                                                    <div className="name-pos">
                                                        <h6 className="name">Abror Po'latov</h6>
                                                        {/*<span className="position">Delicates Studio</span>*/}
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        className="themesflat-testimonials style-1 clearfix image-circle">
                                        <div className="item">
                                            <div className="inner">
                                                <div className="thumb">
                                                    <img
                                                        src="assets/img/testimonials/otziv5.png"
                                                        alt="ImageHome"/>
                                                </div>

                                                <blockquote className="text" style={{height: 350}}>
                                                    <div className="start">
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                    </div>
                                                    <p>
                                                        Bizlarda ham shunday rivojlangan platforma tuzilganiga qoyil
                                                        qoldim .
                                                        Kirishim bilan yordamchi savol qabul qilib oldi ,hamma taraflama
                                                        spetsialistlar bor ekan...
                                                        Menga shifokor maslahati kerak edi va bu platforma orqali
                                                        bolamni bezovta qilayotgan narsaga yechim topdim.</p>
                                                    <div className="name-pos">
                                                        <h6 className="name">Madina Yakubova</h6>
                                                        {/*<span className="position">Delicates Studio</span>*/}
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        className="themesflat-testimonials style-1 clearfix image-circle">
                                        <div className="item">
                                            <div className="inner">
                                                <div className="thumb">
                                                    <img
                                                        src="assets/img/testimonials/otziv1.png"
                                                        alt="ImageHome"/>
                                                </div>
                                                <blockquote className="text" style={{height: 350}}>
                                                    <div className="start">
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                    </div>
                                                    <p>
Salom.Sizlarga katta raxmat , Turmush o’rtog’im bilan ko’pincha kelishmay qolar edik, bu saytdagi maslahatlarni hayotda ishlatishni boshlaganimdan beri mojarolarga nuqta qo’yildi..bir birimizga bo’lgan mehr yana oldingi paytlardek , farzandim tug’ilgandan beri hali unday bo’lmagan edi</p>
                                                    <div className="name-pos">
                                                        <h6 className="name">Og’iloy</h6>
                                                        {/*<span className="position">Delicates Studio</span>*/}
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        className="themesflat-testimonials style-1 clearfix image-circle">
                                        <div className="item">
                                            <div className="inner">
                                                <div className="thumb">
                                                    <img
                                                        src="assets/img/testimonials/otziv3.png"
                                                        alt="ImageHome"/>
                                                </div>

                                                <blockquote className="text" style={{height: 350}}>
                                                    <div className="start">
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                    </div>
                                                    <p>
                                                        Привет всем.
                                                        Очень рекомендую эту платформу, они есть почти что во всех соц сетях вы можете там подписаться .. у них проходят классные и полезные мастер классы и семинары.Спасиба за такое платформу.Спасиба за такое платформу....</p>
                                                    <div className="name-pos">
                                                        <h6 className="name">Надира Бекмурадова</h6>
                                                        {/*<span className="position">Delicates Studio</span>*/}
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        className="themesflat-testimonials style-1 clearfix image-circle">
                                        <div className="item">
                                            <div className="inner">
                                                <div className="thumb">
                                                    <img
                                                        src="assets/img/testimonials/otziv6.png"
                                                        alt="ImageHome"/>
                                                </div>

                                                <blockquote className="text" style={{height: 350}}>
                                                    <div className="start">
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                        <i className="finance-icon-star"></i>
                                                    </div>
                                                    <p>
                                                        
                                                        Assalomu aleykum, raxmat shu platformela orqali yengi chiqgan qonunlani ko’rib, tanishib o’zimga kereli ma’lumotlaga ega bo’ldim.Bizlarda ham shunday rivojlangan platforma tuzilganiga qoyil
                                                        qoldim .
                                                        Baraka topila!
                                                        Menga shifokor maslahati kerak edi va bu platforma orqali
                                                        bolamni bezovta qilayotgan narsaga yechim topdim.
                                                        </p>
                                                    <div className="name-pos">
                                                        <h6 className="name">O’tkir</h6>
                                                        {/*<span className="position">Delicates Studio</span>*/}
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div className="themesflat-spacer clearfix" data-desktop="32"
                                 data-mobi="0" data-smobi="0"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    );
}

export default HomeTestimonial;
import React from 'react';
import '../../i18n'
import {useTranslation} from "react-i18next";

function HomeVideo() {
    const {t, i18} = useTranslation();
    return (
        <div>
            <div className="row-video">
                <div className="container">
                    <div className="themesflat-spacer clearfix" data-desktop="104"
                         data-mobi="60" data-smobi="60"></div>
                    <div className="row equalize sm-equalize-auto">
                        <div className="col-md-6">
                            <div className="themesflat-spacer clearfix" data-desktop="33"
                                 data-mobi="0" data-smobi="0"></div>

                            <div className="themesflat-headings style-1 clearfix" style={{paddingRight: "15px"}}>
                                <h2 className="heading letter-spacing--09px clearfix"
                                    style={{marginTop: "20px"}}>Почему важно знать свои права?</h2>
                                <div className="sep clearfix"></div>
                                <p className="sub-heading clearfix">
                                    В первую очередь человеку необходимо знать свои
                                    <br/>
                                    права для того, чтобы иметь возможность их отстоять, опираясь на определенные законы
                                    и положения, ведь невозможно узнать, что права нарушены в случае их незнания.
                                    <br/>
                                    Также
                                    знание законов помогает не нарушать их самому.
                                    В повседневной жизни каждый человек должен знать и защищать свои права.

                                    <br/>
                                    Цель платформы – это оказание юридических услуг в сфере гражданских правоотношений,
                                    а также продвижение юридической грамотности в массы. На сайте можно получить
                                    юридическую консультацию онлайн или помощь в грамотном оформлении юридических
                                    документов
                                </p>
                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="20"
                                 data-mobi="20" data-smobi="20"></div>

                            <div className="themesflat-spacer clearfix" data-desktop="21"
                                 data-mobi="20" data-smobi="20"></div>
                            <div className="themesflat-spacer clearfix" data-desktop="28"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>


                        <div className="col-md-6 half-background style-1 huquq">

                        </div>

                    </div>
                    <div className="themesflat-spacer clearfix" data-desktop="80" data-mobi="60"
                         data-smobi="60"></div>
                    <div className="border-h-1 dark"></div>
                </div>
            </div>

            <div className="row-video">
                <div className="container">
                    <div className="themesflat-spacer clearfix" data-desktop="104"
                         data-mobi="60" data-smobi="60"></div>
                    <div className="row equalize sm-equalize-auto">
                        <div className="col-md-6 half-background style-1 religin">

                        </div>

                        <div className="col-md-6 " style={{paddingLeft: "30px"}}>
                            <div className="themesflat-spacer clearfix" data-desktop="33"
                                 data-mobi="0" data-smobi="0"></div>

                            <div className="themesflat-headings style-1 clearfix">
                                <h2 className="heading letter-spacing--09px clearfix" style={{marginTop: "20px"}}>В чём
                                    состоит роль религии ?</h2>
                                <div className="sep clearfix"></div>
                                <p className="sub-heading clearfix">
                                    Религия прежде всего является для каждого члена
                                    общества одной из возможностей познать мир под определенным углом: опираясь на
                                    систему принципов, идеалов, убеждений, которые она предлагает.
                                    <br/>
                                    <br/>
                                    Для большинства верующих религия является утешением, надеждой, опорой, что
                                    положительно влияет на общество в целом.
                                    <br/>
                                    <br/>
                                    Посредством системы ценностей религии возможен контроль над обществом, что не всегда
                                    является отрицательным моментом, т.к. большинство массовых религий предлагают
                                    положительную с человеческой точки зрения систему ценностей, моральных
                                    установок.</p>
                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="20"
                                 data-mobi="20" data-smobi="20"></div>

                            <div className="themesflat-spacer clearfix" data-desktop="21"
                                 data-mobi="20" data-smobi="20"></div>
                            <div className="themesflat-spacer clearfix" data-desktop="28"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>


                    </div>
                    <div className="themesflat-spacer clearfix" data-desktop="80" data-mobi="60"
                         data-smobi="60"></div>
                    <div className="border-h-1 dark"></div>
                </div>
            </div>


            <div className="row-video">
                <div className="container">
                    <div className="themesflat-spacer clearfix" data-desktop="104"
                         data-mobi="60" data-smobi="60"></div>
                    <div className="row equalize sm-equalize-auto">
                        <div className="col-md-6">
                            <div className="themesflat-spacer clearfix" data-desktop="33"
                                 data-mobi="0" data-smobi="0"></div>

                            <div className="themesflat-headings style-1 clearfix" style={{paddingRight: "15px"}}>
                                <h2 className="heading letter-spacing--09px clearfix" style={{marginTop: "20px"}}>Зачем
                                    нужно изучать
                                    психологию?</h2>
                                <div className="sep clearfix"></div>
                                <p className="sub-heading clearfix">Понимание психологии это
                                    самопознания а также для понимания других людей и понимания взаимоотношений между
                                    ним Человек с рождения проходит процесс социализации, он растет, развивается,
                                    общается с другими людьми и, конечно же становится частью человеческого общества.
                                    <br/>
                                    <br/>
                                    Поэтому просто необходимо знать психологическое устройство других людей.
                                    В первую очередь, необходимо понять себя, изучить свои слабые и сильные стороны.
                                    <br/>
                                    <br/>
                                    Нужно поставить себе цель и определить приоритеты в жизни, сформировать свою позицию
                                    в обществе, и наконец, воспитать себя.
                                    Изучая собственные особенности, человек начнет внимательней относится к окружающим,
                                    замечать уже их особенности, слабые и сильные места, которые могут помочь в общении,
                                    помогут понять мотивы в поведении и реакции других людей.
                                </p>
                            </div>

                            <div className="themesflat-spacer clearfix" data-desktop="20"
                                 data-mobi="20" data-smobi="20"></div>

                            <div className="themesflat-spacer clearfix" data-desktop="21"
                                 data-mobi="20" data-smobi="20"></div>
                            <div className="themesflat-spacer clearfix" data-desktop="28"
                                 data-mobi="60" data-smobi="60"></div>
                        </div>


                        <div className="col-md-6 half-background style-1 psiholog">

                        </div>

                    </div>
                    <div className="themesflat-spacer clearfix" data-desktop="80" data-mobi="60"
                         data-smobi="60"></div>
                    <div className="border-h-1 dark"></div>
                </div>
            </div>
        </div>
    );
}

export default HomeVideo;
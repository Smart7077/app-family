import React from 'react';
import {GoogleMap, Marker, withGoogleMap, withScriptjs} from "react-google-maps";


function GoogleMaps(props) {
    return (
        <div>
            <GoogleMap
                defaultZoom={12}
                defaultCenter={{lat: 41.320151, lng: 69.262558}}
            >
                <Marker
                    key={1}
                    position={{
                        lat: 41.320151,
                        lng: 69.262558
                    }}

                />
            </GoogleMap>
        </div>
    );
}

const WrappedMap = withScriptjs(withGoogleMap(GoogleMaps))


function Map() {
    return (
        <div style={{width: '100vw%', height: '50vh'}}>
            <WrappedMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDhYZ0AImn2EBDty7kCAHWDWbAsZ7JtL-M`}
                loadingElement={<div style={{height: `100%`}}/>}
                containerElement={<div style={{height: `400px`}}/>}
                mapElement={<div style={{height: `100%`}}/>}
            />
        </div>
    );
}

export default Map;






export default function FooterTegs() {

    return (
        <div className="span_1_of_4 col">
            <div className="widget widget_tag_cloud margin-top-6 padding-left-9">
                <h2 className="widget-title margin-bottom-43"><span>Популярные теги</span></h2>

                <div className="tagcloud">
                    <a href="/yearToOne">Семья</a>
                    <a href="/credit">Кредиты</a>
                    <a href="/yearToOne">Воспитание</a>
                    <a href="/milkBaby">Дети</a>
                    <a href="/facility">Льготы</a>
                    <a href="/religion">Религия</a>
                    <a href="/facility">Право</a>
                    <a href="/psychology">Психология</a>
                    <a href="/milkBaby">Консультация</a>
                </div>
            </div>
        </div>
    )
}
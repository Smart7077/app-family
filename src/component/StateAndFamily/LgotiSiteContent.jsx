import NewsSingleComments from "../NewsSingle/NewsSingleComments";

const LgotiSiteContent = () => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                        className="site-content clearfix">
                        <div id="inner-content"
                            className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src="assets/img/stateAndFamily/lgoti.jpg"
                                            alt="Image" /></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="page-blog-single.html">Налоговые льготы</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">

                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                                                                </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="post-content post-excerpt">
                                            <p>Некоторые налоговые льготы, предоставляемые молодым семьям, будут отменены.</p>
                                            
                                            <p>Принят Указ Президента «Об отмене раздельных налоговых и таможенных льгот» (ПФ-6011, 19.06.2020). Об этом сообщает канал правовой информации.</p>

                                            <p>Согласно указу:<br/>

                                            налоговые льготы, предусмотренные налоговым законодательством, не распространяются на социальный налог, если в них прямо не указано освобождение от этого налога;</p>

                                            <p>Освобождение от НДС работ (услуг), приобретенных у юридических лиц-нерезидентов Узбекистана, не распространяется на работы (услуги), выполняемые юридическими лицами-нерезидентами, осуществляющими деятельность в Узбекистане через постоянные представительства.</p>

                                            <p> Также в соответствии с Указом вносятся изменения в ряд указов и постановлений Президента, отменяется ряд налоговых льгот.</p>

                                           <p>В частности, налогооблагаемая заработная плата и другие доходы молодых членов семьи, установленные Указом Президента РФ № PF-3878, вычитаются из подоходного налога для покрытия ипотечных кредитов и начисленных по ним процентов за строительство, ремонт и покупку квартиры в частном доме. или многоквартирный дом. Льготная норма будет отменена с 1 января 2021 года.</p>

                                            <p>Указ вступит в силу 1 октября 2020 года.</p>

                                            
                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="" rel="tag">Кредиты</a>
                                            <a href="" rel="tag">Право</a>
                                            <a href=""
                                                rel="tag">Консультация</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments />
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default LgotiSiteContent;
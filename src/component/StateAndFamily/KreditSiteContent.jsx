import NewsSingleComments from "../NewsSingle/NewsSingleComments";

const KreditSiteContent = () => {

    return (

        <div className="col-md-9">
            <div id="main-content" className="site-main clearfix">
                <div id="content-wrap" className="themesflat-container">
                    <div id="site-content"
                        className="site-content clearfix">
                        <div id="inner-content"
                            className="inner-content-wrap">
                            <article className="hentry">
                                <div className="post-content-single-wrap">
                                    <div className="post-media clearfix">
                                        <a href="#"><img
                                            src="assets/img/stateAndFamily/kredit.jpg"
                                            alt="Image" /></a>
                                    </div>

                                    <div className="post-content-wrap">
                                        <h2 className="post-title">
                                            <span className="post-title-inner">
                                                <a href="page-blog-single.html">Кредит</a>
                                            </span>
                                        </h2>

                                        <div className="post-meta style-1">
                                            <div
                                                className="post-meta-content">

                                                <ul className="post-date">
                                                    <li className="date">
                                                        <span
                                                            className="day"> 09 </span>
                                                    </li>
                                                    <li className="month">
                                                        NOV
                                                                                                </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="post-content post-excerpt">
                                            <p>Срок подачи заявок на субсидию по ипотечному кредиту: с 1 марта по 1 октября 2021 года. Заявки, поданные по истечении этого срока, рассматриваться не будут.</p>
                                            
                                            <p>С 1 марта текущего года начался процесс выделения субсидий малообеспеченным гражданам на 2021 год и нуждающимся в улучшении жилищных условий.</p>
                                            
                                            <p>Для получения субсидии необходимы следующие документы:</p>

                                            <p>- заграничный пасспорт;</p>

                                            <p>- Паспорт членов семьи и свидетельство о браке (для полной семьи и совершеннолетнего ребенка (ов)) и копия свидетельства о рождении детей (если несовершеннолетний);</p>

                                            <p>- Свидетельство о расторжении брака или решение суда о расторжении брака (в случае регистрации брака) и копия справки о семейном положении из архива ЗАГСа (в случае нерегистрации брака);</p>

                                            <p>- Информация о кадастровом номере и типе постоянного проживания;
</p>
                                            <p>- копия домовой книги или квартирной карты (согласно рисунку 17) с места постоянного проживания;</p>
                                            
                                            <p>- Справка о доходах и удержанных налогах на прибыль заявителя и созаемщика (налоговая декларация в случае дополнительного дохода).</p>

                                            <p>Населению будут предоставлены ипотечные кредиты сроком на 20 лет с льготным периодом 6 месяцев.</p>

                                            <p>Самый простой способ получить субсидию - подать заявку через портал электронного правительства my.gov.uz.</p>
                                            
                                            <p>В случае положительного ответа гражданин сможет получить субсидию по своему выбору в виде денежных средств для покрытия части первоначального взноса или выплаты процентов по кредиту на покупку жилья.</p>

                                            <h6>Вы можете получить официальную информацию о потребительских погашениях здесь:</h6>

                                            <p>
                                                <a href="https://bank.uz/uz/credits/potrebitelskiy-kredit">Потребительские кредиты до 2021 года. Топ-10 кредитов по процентной ставке</a>
                                            </p>

                                            <p>
                                                <a href="https://bank.uz/uz/ipoteka">Ипотечные кредиты 2021. Самые выгодные проценты по ипотеке</a>
                                            </p>
                                        </div>

                                        <div className="post-tags clearfix">
                                            <span>Теги:</span>
                                            <a href="" rel="tag">Кредиты</a>
                                            <a href="" rel="tag">Право</a>
                                            <a href=""
                                                rel="tag">Консультация</a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <NewsSingleComments />
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default KreditSiteContent;